angular.module('PatientRecord.heightCtrl',[])
.controller('heightCtrl', function($scope, $state, $ionicHistory, customService, user, $ionicLoading, customService, $ionicPopup, $ionicPopover, $timeout, disease, $rootScope, $filter, $location, $ionicScrollDelegate, global) {    
    $scope.$on("$ionicView.beforeEnter", function() {
        $scope.user = {"userId":customService.userDBInfo.userId,"feet":"","inch":"","cmVal":"","note":"","recordate":new Date(),"fromDate":undefined,"toDate":undefined};
        $scope.userInfo = angular.copy(customService.userDBInfo);
        $scope.notesLbl = true;
        $scope.editNotesLbl = false;
        $scope.heightData = [];
        $scope.userName = customService.userDBInfo.fullName;
        $scope.onHoldPressed = false;
        $scope.arrForShare = [];
        $scope.shareBtn = true;
        $scope.moreBtn = false;
        $scope.oneWFilter = false;
        $scope.oneMFilter = false;
        $scope.threeMFilter = false;
        $scope.sixMFilter = false;
        $scope.oneYFilter = false;
        if(customService.userDBInfo.image==null || customService.userDBInfo.image==""){
            $scope.userimage = 'img/profile_pic.png';
        } else { 
            $scope.userimage = customService.userDBInfo.image;
        }
        $scope.settingData = {};
        disease.getPatientSettingData(localStorage.getItem('loginUserId')).then(function(objS){
            //console.log("getPatientSettingData_succ->>  "+JSON.stringify(objS));              
            $scope.settingData = objS;           
        },function(objE){
            console.log("getPatientSettingData_err->>  "+JSON.stringify(objE));
        });
        $scope.getHeightData();
        $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
        $timeout(function(){
            var id=$('.date-time_bp-P').attr('id');
            $('#'+id)[0].value=$filter('dateWithTime')($scope.user.recordate);  
            $ionicLoading.hide();      
        },2000);
    });
    
    $scope.getHeightData = function() {
        console.log('call')
        user.fetchHeight().then(function(res){
            //console.log('succ: '+JSON.stringify(res));
            $scope.graphRecord = [['Height', 'Entry']];
            //$scope.heightData = res;
            $scope.heightData = [];
            for(var i=0; i<res.length; i++){
                if(res[i].feet!="" || res[i].inch!="")
                    $scope.heightData.push(res[i]);
            }
            for(i=0;i<$scope.heightData.length;i++) {
                var timeValue=new Date($scope.heightData[i].recordDate);
                var timeFormat=timeValue.getDate()+'-'+(timeValue.getMonth()+1)+'-'+timeValue.getFullYear();
                $scope.heightData[i].centimeter =  parseInt($scope.heightData[i].cmVal);
                $scope.graphRecord.push([timeFormat,$scope.heightData[i].centimeter])
            }
            for(var i=0; i<$scope.heightData.length; i++){
                $scope.heightData[i].selected = false;
            }
            if($scope.graphRecord.length>1) {
                $timeout(function(){                    
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                },1000);
            }
            function drawChart() {
                var data = google.visualization.arrayToDataTable($scope.graphRecord);
                var options = {
                    title: 'Height Record',
                    hAxis: {title: 'Time',  
                        titleTextStyle: {color: '#333'},
                        direction: -1, 
                        slantedText: true, 
                        slantedTextAngle: 90
                    },
                    vAxis: {title: 'CM',minValue: 0,gridlines: {count: 9}},
                    legend: {position: 'none'}
                };

                var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            }            
            $location.hash(global.heightId);
            $ionicScrollDelegate.$getByHandle('heightList').anchorScroll(true);
            //console.log('Height Data--->>>: '+JSON.stringify($scope.heightData));
        },function(err){
            console.log('err: '+JSON.stringify(res));
        })
    }
    
    var numRegxForSpecial = (/^[0-9]*$/);
    var numRegxForDot = (/^[0-9.]*$/);
    $scope.addHeightRecord = function() {
        if($scope.settingData.heightUnit=="ftInch"){
            if($scope.user.feet=='' || $scope.user.feet==0) {
                customService.ToShowAlert('Please enter height in feet.')
                return;
            }
            if(!numRegxForSpecial.test($scope.user.feet)){
                customService.ToShowAlert('Please enter valid height in feet.')
                return;
            }
            if($scope.user.inch=='') {
                customService.ToShowAlert('Please enter height in inch.')
                return;
            }
            if($scope.user.inch!='' && !numRegxForSpecial.test($scope.user.inch)){
                customService.ToShowAlert('Please enter valid height in inch.')
                return;
            }
            if($scope.user.inch>11){
                customService.ToShowAlert('Height cannot be more than 11 inch.')
                return;
            }
            if($scope.user.recordate=='') {
                customService.ToShowAlert('Please select date for height.')
                return;
            }
        }
        if($scope.settingData.heightUnit=="cm"){
            if($scope.user.cmVal=='' || $scope.user.cmVal==0) {
                customService.ToShowAlert('Please enter height in cm.')
                return;
            }
            if(($scope.user.cmVal!="" && !numRegxForDot.test($scope.user.cmVal)) || $scope.user.cmVal.split('.').length>2){
                customService.ToShowAlert('Please enter valid height in cm.')
                return;
            }
            if($scope.user.recordate=='') {
                customService.ToShowAlert('Please select date for height.')
                return;
            }
        }
        if($scope.user.feet!='' && $scope.user.feet!=0 && $scope.user.inch!=''){
            //$scope.user.cmVal = ($scope.user.feet*12+$scope.user.inch)*2.54;
            $scope.user.cmVal = ($scope.user.feet*30.48)+($scope.user.inch*2.54);
        }
        else if($scope.user.cmVal!='' && $scope.user.cmVal!=0){
            var inches = ($scope.user.cmVal*0.393700787).toFixed(0);
            var feet = Math.floor(inches / 12);
            inches %= 12;
            $scope.user.feet = feet;
            $scope.user.inch = inches;
        }       
        /*if(typeof $scope.user.recordate=='object')
            $scope.user.recordate = $scope.user.recordate.getTime();*/
        $scope.user.recordate = $scope.user.recordate.getTime();
        //console.log("addHeightRecord->>  "+JSON.stringify($scope.user));
        user.insertHeight($scope.user).then(function(objS) {
            user.fetchLatestData("userHeight","recordDate").then(function(latSucc){
                //console.log("latSucc=>>  "+JSON.stringify(latSucc));
                user.updateData('users','server_userID',['heightPri','heightSec','heightType','heightCM', 'heightCMType','heightDate', 'updtDate', 'sync'],[latSucc.ftVal,latSucc.inchVal,'Inch',latSucc.cmVal,'cm',latSucc.recordDate,new Date().getTime(),"0",$scope.user.userId]).then(function(succ) {
                    console.log("updateData->> "+JSON.stringify(succ));
                    user.fetchUserInfo().then(function(userSucc){
                        customService.userDBInfo = userSucc;
                        $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
                        $timeout(function(){
                            $ionicLoading.hide();
                            $state.reload();
                        },1000);                    
                    },function(userErr){
                        console.log(JSON.stringify(userErr));
                    });
                },function(err) {
                    console.log(JSON.stringify(err));
                });
            },function(latErr){
                console.log("latErr=>>  "+JSON.stringify(latErr));
            });            
        },function(objE){
        
        });
        $scope.lastNoteVal = ''
    }
    
    
    $scope.lastNoteVal = ''
    $scope.addNote = function(){
        $scope.heightNote = $scope.lastNoteVal;
        var myPopup = $ionicPopup.show({
          template: '<div class="popup-content"><div class="textarea-block"><textarea placeholder="Notes" ng-model="heightNote"></textarea></div><div class="row btn-popup"><div class="col"><button class="button button-block btn-green" ng-click="saveNotes(heightNote)">Save</button></div><div class="col"><button class="button button-block btn-green" ng-click="closepop()"> Cancel </button></div></div></div>',
          scope: $scope,
          cssClass:'notes-popup'
        });
        $scope.closepop = function(){myPopup.close();}
        $scope.saveNotes = function(val){
            $scope.user.note = val;
            $scope.lastNoteVal = val;
            $scope.notesLbl = false;
            $scope.editNotesLbl = true;
            myPopup.close();
        }
    }
    
    $scope.myGoBack = function() {
        //$ionicHistory.goBack();
        //$state.go($rootScope.previousState);
        if(customService.preStateForWHW=="menu"){
            $state.go('menu.home');
        }else if(customService.preStateForWHW=="personal"){
            $state.go('personal');
        }else{
            $state.go('personal');
        }
    }
    
    $scope.heightNotes = function(data) {            
        $scope.savedNote = data;
        var myPopup = $ionicPopup.show({
          template: '<div class="popup-content"><div class="textarea-block"><textarea placeholder="Notes" ng-model="savedNote" readonly></textarea></div><div class="row btn-popup"><div class="col"><button class="button button-block btn-green" ng-click="closepop()">Close</button></div></div></div>',
          scope: $scope,
          cssClass:'notes-popup'
        });
        $scope.closepop = function(){myPopup.close();}
    }

    $scope.onHold = function(obj,ind){
        $scope.onHoldPressed = true;
        obj.selected = true;
        if(obj.selected == true)
            $scope.arrForShare.push(obj);
        //console.log("arrForShare->>  "+JSON.stringify($scope.arrForShare));
        if($scope.arrForShare.length>0){
            $scope.shareBtn=false;
            $scope.moreBtn=true;
        }else{
            $scope.shareBtn=true;
            $scope.moreBtn=false;
        }
    }

    $scope.onTap = function(obj,ind){
        if($scope.onHoldPressed == true)
            obj.selected==true?obj.selected=false:obj.selected=true;
        var isSelected = false;
        for(var i=0; i<$scope.heightData.length; i++){
            if($scope.heightData[i].selected==true){
                isSelected = true;
                break;
            }else{
                isSelected = false;
            }
        }
        isSelected == false?$scope.onHoldPressed = false:$scope.onHoldPressed = true;
        if(obj.selected == false){
            for(var i=0; i<$scope.arrForShare.length; i++){
                if($scope.arrForShare[i].recordDate==obj.recordDate){
                    $scope.arrForShare.splice($scope.arrForShare.indexOf($scope.arrForShare[i]), 1);
                }
            }
        }else{
            $scope.arrForShare.push(obj);
        }
        var anySelected = false;
        for(var i=0; i<$scope.heightData.length; i++){
            if($scope.heightData[i].selected==true){
                anySelected = true;
                break;
            }else{
                anySelected = false;
            }
        }
        if(anySelected==true){
            $scope.shareBtn=false;
            $scope.moreBtn=true;
        }else{
            $scope.shareBtn=true;
            $scope.moreBtn=false;
        }
        //console.log("arrForShare->>  "+JSON.stringify($scope.arrForShare));
    }

    /* Open popover on more option */
    $ionicPopover.fromTemplateUrl('popover.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
    });

    $scope.openShareOpt = function($event){
        $scope.popover.show($event);
    }

    $scope.actionPerform = function(act){
        console.log("action->>  "+act);
        if(act=="Delete"){
            //console.log("delSelectedArr->>  "+JSON.stringify($scope.arrForShare));      
            var statement = "";  
            for(var i=0; i<$scope.heightData.length; i++){
                if($scope.heightData[i].selected==true)
                    statement = statement + $scope.heightData[i].uniId+",";
            }  
            statement = statement.substring(0,statement.length-1); 
            var query = "update userHeight set status='inactive', updtDate="+new Date().getTime()+", sync='0' where uniId in ("+statement+")";
            user.updateMulRowsAndColm(query).then(function(objS){
                $scope.onHoldPressed = false;
                $scope.shareBtn = true;
                $scope.moreBtn = false;
                $scope.getHeightData();
                $scope.popover.hide();
                user.getFirstRowFromTbl('userHeight').then(function(objS){
                    console.log("userHeight_firstRow->> "+JSON.stringify(objS));
                    if(objS.length>0){
                        user.updateData('users','server_userID',['heightPri','heightSec','heightType','heightCM', 'heightCMType','heightDate', 'updtDate', 'sync'],[objS[0].ftVal,objS[0].inchVal,'Inch',objS[0].cmVal,'cm',objS[0].recordDate,new Date().getTime(),"0",$scope.user.userId]).then(function(succ) {
                            console.log("updateData->> "+JSON.stringify(succ));
                            user.fetchUserInfo().then(function(userSucc){
                                customService.userDBInfo = userSucc;
                            },function(userErr){
                                console.log(JSON.stringify(userErr));
                            });
                        },function(err) {
                            console.log(JSON.stringify(err));
                        });
                    }else{
                        user.updateData('users','server_userID',['heightPri','heightSec','heightType','heightCM', 'heightCMType','heightDate', 'updtDate', 'sync'],['','','','','','',new Date().getTime(),'0',$scope.user.userId]).then(function(succ) {
                            console.log("updateData->> "+JSON.stringify(succ));
                            user.fetchUserInfo().then(function(userSucc){
                                customService.userDBInfo = userSucc;
                            },function(userErr){
                                console.log(JSON.stringify(userErr));
                            });
                        },function(err) {
                            console.log(JSON.stringify(err));
                        });
                    }
                },function(objE){
                    console.log("userHeight_firstRow->> "+JSON.stringify(objE));
                });
            },function(objE){
            });            
        }
        if(act=="SelectAll"){
            for(var i=0; i<$scope.heightData.length; i++){
                $scope.heightData[i].selected = true;
            }
            $scope.popover.hide();
        }
        if(act=="DeselectAll"){
            for(var i=0; i<$scope.heightData.length; i++){
                $scope.heightData[i].selected = false;
            }
            $scope.shareBtn = true;
            $scope.moreBtn = false;
            $scope.onHoldPressed = false;
            $scope.popover.hide();
        }
        if(act=="Share"){
            //console.log("shareAllArr->>  "+JSON.stringify($scope.heightData));            
            $scope.popover.hide();
        }        
    }

    $scope.compareDate = function(){
        console.log("fromDate->> "+$scope.user.fromDate);
        console.log("toDate->> "+$scope.user.toDate);        
        if($scope.user.toDate!=0 &&($scope.user.fromDate>$scope.user.toDate)){
            customService.ToShowAlert('To date must be greater than or equal to from date.');
            //$scope.user.toDate = 0;
            $scope.user.toDate = undefined;
            $scope.user.fromDate = undefined;
            $state.reload();
            return;
        }
        if($scope.user.fromDate!=0 && $scope.user.toDate>=$scope.user.fromDate){
            console.log("done");
            user.selectHeightData('userHeight',$scope.user.fromDate.getTime(),$scope.user.toDate.getTime()).then(function(objS){
                console.log("selectHeightData_succ->>  "+JSON.stringify(objS));
                $scope.heightData = objS;
                $scope.graphRecord = [['Height', 'Entry']];
                for(i=0;i<$scope.heightData.length;i++) {
                    var timeValue=new Date($scope.heightData[i].recordDate);
                    //var timeFormat=timeValue.getDate()+'-'+timeValue.getMonth()+'-'+timeValue.getFullYear()+'-'+timeValue.getHours()+':'+timeValue.getMinutes()+':'+timeValue.getSeconds();
                    var timeFormat=timeValue.getDate()+'-'+(timeValue.getMonth()+1)+'-'+timeValue.getFullYear();
                    //$scope.heightData[i].centimeter = parseInt(parseInt($scope.heightData[i].feet)*30.48+parseInt($scope.heightData[i].inch)*2.54); 
                    $scope.heightData[i].centimeter =  parseInt($scope.heightData[i].cmVal);
                    $scope.graphRecord.push([timeFormat,$scope.heightData[i].centimeter])
                    //console.log('graphRecord -> '+JSON.stringify($scope.graphRecord));
                }
                for(var i=0; i<$scope.heightData.length; i++){
                    $scope.heightData[i].selected = false;
                }
                if($scope.graphRecord.length>1) {
                    $timeout(function(){                    
                        google.charts.load('current', {'packages':['corechart']});
                        google.charts.setOnLoadCallback(drawChart);
                    },500);
                }
                function drawChart() {
                    var data = google.visualization.arrayToDataTable($scope.graphRecord);
                    var options = {
                        title: 'Height Record',
                        hAxis: {title: 'Time',  
                            titleTextStyle: {color: '#333'},
                            direction: -1, 
                            slantedText: true, 
                            slantedTextAngle: 90
                        },
                        vAxis: {title: 'CM',minValue: 0,gridlines: {count: 9}},
                        legend: {position: 'none'}
                    };
                    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                    chart.draw(data, options);
                }
            },function(objE){
                console.log("selectHeightData_err->>  "+JSON.stringify(objE));
            });
        }
    }

    /* Mobiscroll Timepicker functionality */
    $scope.settings_date = {
        theme: 'mobiscroll',
        //display: 'bottom',
        max: new Date(),
        dateFormat : "dd/mm/yy",
        timeFormat: 'hh:ii A',
        timeWheels: 'hhii A',
        animate: false,
        steps: { minute: 1, zeroBased: true },
        onSet:function(){
            var id=$('.date-time_bp-P').attr('id');
            $('#'+id)[0].value=$filter('dateWithTime')($scope.user.recordate.getTime());
        }
    };

    $scope.settings_from = {
        theme: 'mobiscroll',
        //display: 'bottom',
        max: new Date(),
        dateFormat : "dd/mm/yy",
        timeFormat: 'hh:ii A',
        timeWheels: 'hhii A',
        animate: false,
        steps: { minute: 1, zeroBased: true },
        onSet:function(){
            console.log("from->>  "+$scope.user.fromDate.getTime());
            var id=$('.date_picker_from').attr('id');
            $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
            $scope.compareDate();
        }
    };

    $scope.settings_to = {
        theme: 'mobiscroll',
        //display: 'bottom',
        max: new Date(),
        dateFormat : "dd/mm/yy",
        timeFormat: 'hh:ii A',
        timeWheels: 'hhii A',
        animate: false,
        steps: { minute: 1, zeroBased: true },
        onSet:function(){
            console.log("to->>  "+$scope.user.toDate.getTime());
            var id=$('.date_picker_to').attr('id');
            $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
            $scope.compareDate();
        }
    };

    $scope.quickFilter = function(type){
        console.log(type)
        if(type=="1wk"){
            $scope.oneWFilter = true;
            $scope.oneMFilter = false;
            $scope.threeMFilter = false;
            $scope.sixMFilter = false;
            $scope.oneYFilter = false;
            var oneWeekAgo = new Date();
            oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);
            $scope.user.fromDate = oneWeekAgo;
            $scope.user.toDate = new Date();
            $timeout(function(){
                var id=$('.date_picker_to').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
                var id=$('.date_picker_from').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
                $scope.compareDate();
            },10);       
        }
        if(type=="1mo"){
            $scope.oneWFilter = false;
            $scope.oneMFilter = true;
            $scope.threeMFilter = false;
            $scope.sixMFilter = false;
            $scope.oneYFilter = false;
            var oneMonthAgo = new Date();
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);
            $scope.user.fromDate = oneMonthAgo;
            $scope.user.toDate = new Date();
            $timeout(function(){
                var id=$('.date_picker_to').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
                var id=$('.date_picker_from').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
                $scope.compareDate();
            },10);
        }
        if(type=="3mo"){
            $scope.oneWFilter = false;
            $scope.oneMFilter = false;
            $scope.threeMFilter = true;
            $scope.sixMFilter = false;
            $scope.oneYFilter = false;
            var oneMonthAgo = new Date();
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 3);
            $scope.user.fromDate = oneMonthAgo;
            $scope.user.toDate = new Date();
            $timeout(function(){
                var id=$('.date_picker_to').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
                var id=$('.date_picker_from').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
                $scope.compareDate();
            },10);
        }
        if(type=="6mo"){
            $scope.oneWFilter = false;
            $scope.oneMFilter = false;
            $scope.threeMFilter = false;
            $scope.sixMFilter = true;
            $scope.oneYFilter = false;
            var oneMonthAgo = new Date();
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 6);
            $scope.user.fromDate = oneMonthAgo;
            $scope.user.toDate = new Date();
            $timeout(function(){
                var id=$('.date_picker_to').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
                var id=$('.date_picker_from').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
                $scope.compareDate();
            },10);
        }
        if(type=="1yr"){
            $scope.oneWFilter = false;
            $scope.oneMFilter = false;
            $scope.threeMFilter = false;
            $scope.sixMFilter = false;
            $scope.oneYFilter = true;
            var oneMonthAgo = new Date();
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 12);
            $scope.user.fromDate = oneMonthAgo;
            $scope.user.toDate = new Date();
            $timeout(function(){
                var id=$('.date_picker_to').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
                var id=$('.date_picker_from').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
                $scope.compareDate();
            },10);
        }
    }

    $scope.clearAllFilters = function(){
        $scope.oneWFilter = false;
        $scope.oneMFilter = false;
        $scope.threeMFilter = false;
        $scope.sixMFilter = false;
        $scope.oneYFilter = false;
        $scope.user.toDate = undefined;
        $scope.user.fromDate = undefined;
        $scope.getHeightData();
    }

    $rootScope.hwBackBtnPressed = function(){
        console.log("backBtnPressed_called->>>  ");
        $(".mbsc-fr-btn1").click();
        $scope.myGoBack();
    }
});









