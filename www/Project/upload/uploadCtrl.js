angular.module('PatientRecord.uploadCtrl',[])
.controller('uploadCtrl', function($scope, $state, DBInstance, $filter, customService, $ionicPopover, $cordovaActionSheet, $cordovaCapture, $cordovaImagePicker, $cordovaFile, $q, user, $rootScope, $ionicLoading, $cordovaFileTransfer, $cordovaFileOpener2, docs, $cordovaCamera) {   
    var fs;
    var count=0,images,base64;
    $scope.$on("$ionicView.beforeEnter", function() {
    	$scope.user = {
    		selCategory:"selCat",
    		providerName:"",
    		title:"",
    		//reportDate:"",
    		notes:""
    	};
        $scope.userInfo = angular.copy(customService.userDBInfo);
    	window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
            fs = fileSystem;
            console.log("filesystem: "+JSON.stringify(fs));
        }, function(err){
            console.log("errorwhile opening file system: ");
        });
    	//$scope.reportDate = 0;
        $scope.gridImageArr = [];
        $scope.uploadDocArr = [];
        $scope.docPathArr=[];
        $scope.arrAfterCopy = [];
        $scope.totalImage = 0;
        $scope.totalUploadedImage = 0;
        $scope.base64ImageArr = [];
        $scope.helCondFiltrData = [];
        console.log("previousState=>>  "+$rootScope.previousState);
        if(customService.typeOfUpload!=""){
            $scope.user.selCategory = customService.typeOfUpload;
        }
        if($rootScope.previousState=="addProvider"){
            console.log("comes from addProvider=>>  "+JSON.stringify(customService.objUpload));
            $scope.providerFiltrData = [];
            $scope.helCondFiltrData = [];
            $scope.user.selCategory = customService.objUpload.category;
            $scope.user.providerName = customService.objUpload.providerName;
            $scope.user.title = customService.objUpload.title;
            //$scope.user.reportDate = customService.objUpload.time;
            $scope.user.notes = customService.objUpload.notes;
            $scope.healthCondArr = customService.objUpload.helCond;
            $scope.gridImageArr = customService.objUpload.uploadPic;
            $scope.docPathArr = customService.objUpload.uploadDoc;
        }else{
            if(customService.uploadRecID!=""){
                $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
                var query = "SELECT * FROM upload_tbl where server_userID="+customService.carouselUserId+" AND delStatus='active' AND uniId="+customService.uploadRecID;
                //console.log("query->>  "+query);
                user.fetchUploadData(query).then(function(objS){
                    $ionicLoading.hide();
                    console.log("fetchUploadData_succ->>  "+JSON.stringify(objS));
                    $scope.getUploadData = objS[0];
                    $scope.providerFiltrData = [];
                    $scope.healthCondArr = [];
                    $scope.user.selCategory = $scope.getUploadData.category;
                    $scope.user.providerName = $scope.getUploadData.provider;
                    $scope.user.notes = $scope.getUploadData.notes;
                    $scope.user.title = $scope.getUploadData.title;
                    var helCopyArr = $scope.getUploadData.helthCond.split(",");
                    var helCopyIdArr = $scope.getUploadData.conditionIds.split(",");
                    for(var i=0; i<helCopyArr.length; i++){
                        $scope.healthCondArr.push({
                            name:helCopyArr[i],
                            id:helCopyIdArr[i] 
                        });
                    }
                    $scope.helCondFiltrData = [];
                    //$scope.user.reportDate = $scope.getUploadData.dateTime;
                    $scope.imgageArr = [];
                    if($scope.getUploadData.uploadPic!=""){
                        if($scope.getUploadData.uploadPic.includes(",") === true){
                            $scope.imgageArr = $scope.getUploadData.uploadPic.split(",");
                        }else{
                            $scope.imgageArr.push($scope.getUploadData.uploadPic);
                        }                
                    }  
                    //$scope.showImageArr = [];  
                    $scope.gridImageArr = [];
                    if($scope.getUploadData.uploadDoc!="" && $scope.getUploadData.uploadDocDir!=""){
                        var docArray=[];
                        var dirArray=[];
                        console.log('doc -> '+$scope.getUploadData.uploadDoc);
                        console.log('dir -> '+$scope.getUploadData.uploadDocDir);
                        if($scope.getUploadData.uploadDoc.includes(",") === true && $scope.getUploadData.uploadDocDir.includes(",") === true){
                            docArray=$scope.getUploadData.uploadDoc.split(',');
                            dirArray=$scope.getUploadData.uploadDocDir.split(',');
                        }else{
                            docArray.push($scope.getUploadData.uploadDoc);
                            dirArray.push($scope.getUploadData.uploadDocDir);
                        }
                        
                        for(var j=0;j<docArray.length;j++){
                            $scope.docPathArr.push({fileDirectory:dirArray[j],oldFileName:docArray[j]});
                        }
                        console.log('docPathArr update -> '+JSON.stringify($scope.docPathArr));
                    }
                    loadImages($scope.imgageArr,$scope.imgageArr.length);
                    /*for(var i=0; i<$scope.imgageArr.length; i++){
                        fs.root.getFile($scope.imgageArr[i], { create: true, exclusive: false }, 
                        function (fileEntry) {
                            console.log("file_actual_path: "+fileEntry.toURL());
                            fileEntry.file(function (file) {
                                var reader = new FileReader();
                                reader.onloadend = function() {
                                    console.log("Successful file read: ",this.result);
                                    $scope.gridImageArr.push({
                                            img:this.result
                                    });
                                    // displayFileData(fileEntry.fullPath + ": " + this.result);
                                    $scope.$apply();
                                };
                                reader.readAsText(file);
                            }, function(err){
                                console.log("error while reading file");
                            });                     

                        },function(err){
                        });
                    }  */  
                },function(objE){
                    $ionicLoading.hide();
                    console.log("fetchUploadData_err->>  "+JSON.stringify(objE));
                });
            }
            else
                $scope.healthCondArr = [{name:"",id:Math.floor(100+Math.random()*900)-2*Math.floor(100+Math.random()*900)}];
        }
        console.log("*****->>   "+JSON.stringify($scope.healthCondArr));
    	/* Functionality for fetching providers list */
        DBInstance.transaction(function(tx){
            var query="select * from userProviders where server_ProviderID=?";
            tx.executeSql(query,[parseInt(localStorage.getItem('loginUserId'))],function(tx,res){
                $scope.providerArr = [];
                for(var i=0; i<res.rows.length;i++){
                    $scope.providerArr.push({
                        id:res.rows.item(i).providerId,
                        //name:res.rows.item(i).firstname+' '+res.rows.item(i).middlename+' '+res.rows.item(i).lastname,
                        name:res.rows.item(i).firstname,
                        role:res.rows.item(i).role,
                        status:res.rows.item(i).status
                    });
                    if(res.rows.item(i).middlename!="")
                        $scope.providerArr[i].name+=' '+res.rows.item(i).middlename;
                    if(res.rows.item(i).lastname!="")
                        $scope.providerArr[i].name+=' '+res.rows.item(i).lastname;
                }      
                //console.log("provider_Arr->> "+JSON.stringify($scope.providerArr));      
            });
        },function(err){
            console.log('userProviders db error');
        },function(){
            console.log('userProviders db succ');
        });
        
        $scope.arrForHlthCondHint=[];
        $scope.copyOfArrForHlthCondHint=[];
        DBInstance.transaction(function(tx){
            var query="select * from userPreexistCondn where status='Active' AND delStatus='active' AND server_userID=?";
            tx.executeSql(query,[customService.userDBInfo.userId],function(tx,res){
                //console.log("userPreexistCondn->>  "+JSON.stringify(res));
                for(var i=0;i<res.rows.length;i++){
                    $scope.arrForHlthCondHint.push({
                        name:res.rows.item(i).preexistName,
                        id:res.rows.item(i).uniId
                    });
                    if(i==res.rows.length-1)
                        $scope.copyOfArrForHlthCondHint=$scope.arrForHlthCondHint;
                }
            });
        },function(err){
            console.log('health condn db error');
        },function(){
            console.log('health condn db succ');
        });

        $scope.completeHelCondArr = [];
        fetchCompleteHelCondFunc();
                
    });
    
    function fetchCompleteHelCondFunc(){
        $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
        DBInstance.transaction(function(tx){
            var query="select * from preexistCondn";
            tx.executeSql(query,[],function(tx,res){
                for(var i=0;i<res.rows.length;i++){
                    $scope.completeHelCondArr.push({
                        name:res.rows.item(i).preexistName,
                        id:res.rows.item(i).uniId
                    });
                }
                //console.log('arrForHlthCondHint -> '+JSON.stringify($scope.arrForHlthCondHint));
            });
        },function(err){
            $ionicLoading.hide();
            console.log('health condn db error');
        },function(){
            $ionicLoading.hide();
            console.log('health condn db succ');
        });
    }

    $scope.helCondFocusFunc = function(){
        $scope.helCondFiltrData = [];
        $scope.helCondFiltrData = $scope.arrForHlthCondHint;
    }

    $scope.helCondBlurFunc = function(){
        $scope.helCondFiltrData =[];
    }

    $scope.providerBlurFunc = function(){
        $scope.providerFiltrData =[];
    }

    var currentInd = 0;
    function loadImages(dataArr,total){
        if(currentInd==0)
            $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
        if (currentInd < total) {
            fs.root.getFile(dataArr[currentInd], { create: true, exclusive: false }, 
            function (fileEntry) {
                //console.log("file_actual_path: "+fileEntry.toURL());
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function() {
                        $scope.gridImageArr.push({
                                img:this.result
                        });
                        currentInd++;
                        return loadImages(dataArr,total);
                        $scope.$apply();
                    };
                    reader.readAsText(file);
                }, function(err){
                    console.log("error while reading file");
                });                     

            },function(err){
            });
        }else{
            console.log('complete recursion');
            $ionicLoading.hide();
        }
    }

    $scope.myGoBack = function() {
        //$state.go('menu.home');
        console.log($rootScope.previousState);
        customService.preStateForAddProvider="";
        $state.go($rootScope.previousState);
    }

    /* Functionality for adding other health condition */
    $scope.addOtherHealthCond = function(){
        var dataFill = false;
        for(var i=0; i<$scope.healthCondArr.length; i++){
            if($scope.healthCondArr[i].name==undefined || $scope.healthCondArr[i].name==""){
                customService.ToShowAlert('Please enter health condition.');
                dataFill = false;
                break;
            }else{ 
                dataFill = true;
            }
        }      
        if(dataFill == true){
            $scope.healthCondArr.push({name:"",id:Math.floor(100+Math.random()*900)-2*Math.floor(100+Math.random()*900)});
            $scope.arrForHlthCondHint = $scope.copyOfArrForHlthCondHint;
        }        
    }

    /* Functionality for hint for provider list */
    $scope.searchProvider = function(val,ind) {
        if(val.trim().length > 1) {
            $scope.providerFiltrData = $filter('filter')($scope.providerArr, val);  
        }
    }
    $scope.setProviderData = function(val){
        console.log('val => '+JSON.stringify(val));
        $scope.user.providerName = val.name;
        $scope.user.providerId = val.id;
        $scope.providerFiltrData = [];        
    }

    /* Functionality for hint for health condition list */
    $scope.searchHealthCond = function(val,ind) {
        if(val.trim().length > 1) {
            $scope.arrForHlthCondHint = $scope.completeHelCondArr;
            $scope.helCondFiltrData = $filter('filter')($scope.arrForHlthCondHint, val); 
        }
    }
    $scope.setHelCondData = function(val,obj){
        obj.name = val.name;
        obj.id = val.id
        $scope.helCondFiltrData = [];
    }


    /* Functionality for uploading images */
    $scope.openOptions = function(){
    	/*var options = {
    		title: 'Choose option',
            buttonLabels: ['Gallery'],
		    addCancelButtonWithLabel: 'Cancel',
		    androidEnableCancelButton : true
  		};
  		$cordovaActionSheet.show(options).then(function(btnIndex) {
        	if(btnIndex==1){
                var options = {
                    maximumImagesCount: 10,
                    width: 800,
                    height: 800,
                    quality: 80
                };
                $cordovaImagePicker.getPictures(options).then(function (results) {
                    for(var i=0; i<results.length; i++){
                        $scope.gridImageArr.push({img:results[i]});
                    }  
                    //$scope.$apply();
                    console.log("gridImageArr->>  "+JSON.stringify($scope.gridImageArr));
                }, function(error) {
                    console.log("getPictures_err->>  "+JSON.stringify(error));
                });
        	}
      	});*/
        var options = {
            quality: 70,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
            //allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation:true
        };
        $cordovaCamera.getPicture(options).then(function(imageURI) {
            console.log("imageURI=>>  "+imageURI);
            $scope.gridImageArr.push({img:imageURI});
        }, function(err) {
            
        });
    }

    var saveImgToFile = function(dataUrl){
        base64 = dataUrl;
        //console.log("base64: ",base64);
        var name = new Date().getTime()+".txt"
        $scope.base64ImageArr.push(name);
        fs.root.getFile(name, { create: true, exclusive: false }, function(fileEntry){
            fileEntry.createWriter(writerCallback);
        }, 
        function(err){
            console.log("errorwhile getting file: "+JSON.stringify(err));
        });    
    }   


    var writerCallback = function(fileWriter){
        fileWriter.onwriteend = fileWriteCallback;
        fileWriter.onerror = function (e) {
            console.log("Failed file write: " + e.toString());
        };
        fileWriter.write(base64);
    }

    var fileWriteCallback = function(){
        //console.log("Successful file write...");
        //console.log("count: "+count);
        count++;
        if(count < $scope.gridImageArr.length)
          user.getBase64Image($scope.gridImageArr[count].img,saveImgToFile);
        else     
          afterImageStore();  
    }
    saveImgToFile = saveImgToFile.bind(this);
    writerCallback = writerCallback.bind(this);
    fileWriteCallback = fileWriteCallback.bind(this);

    $scope.removeImg = function(index){
        $scope.gridImageArr.splice(index, 1);
    }

    $scope.removeDoc = function(index){
        //$scope.uploadDocArr.splice(index, 1);
        $scope.docPathArr.splice(index, 1);
    }


    $scope.openPDF = function(){
        var options = {
            title: 'Choose option',
            buttonLabels: ['Choose Image', 'Choose pdf'],
            //buttonLabels: ['Gallery'],
            addCancelButtonWithLabel: 'Cancel',
            androidEnableCancelButton : true
        };
        $cordovaActionSheet.show(options).then(function(btnIndex) {
            //console.log('btnIndex => '+btnIndex);
            if(btnIndex==1){
                var options = {
                    maximumImagesCount: 10,
                    width: 800,
                    height: 800,
                    quality: 80
                };
                $cordovaImagePicker.getPictures(options).then(function (results) {
                    for(var i=0; i<results.length; i++){
                        $scope.gridImageArr.push({img:results[i]});
                    }  
                    //$scope.$apply();
                    //console.log("gridImageArr->>  "+JSON.stringify($scope.gridImageArr));
                }, function(error) {
                    console.log("getPictures_err->>  "+JSON.stringify(error));
                });
            }else if(btnIndex==2){
                console.log("type->> "+localStorage.getItem('device_type'));
                if(localStorage.getItem('device_type')=="android"){
                    fileChooser.open(function(succ) {
                        console.log("open_succ******* ->  "+succ);
                        window.FilePath.resolveNativePath(succ, function(success){
                            console.log('success path => '+JSON.stringify(success));
                            var sourceDirectory = success.substring(0, success.lastIndexOf('/') + 1);
                            var sourceFileName = success.substring(success.lastIndexOf('/') + 1, success.length);
                            console.log('sourceFileName => '+sourceFileName);
                            if(sourceFileName.split('.')[1] != 'pdf'){
                                customService.ToShowAlert('Please select only pdf files.');
                                return;
                            }
                            $scope.docPathArr.push({fileDirectory:sourceDirectory,oldFileName:sourceFileName,fileName:new Date().getTime()+'.pdf'});
                            $scope.$apply();
                        }, function(error){
                            console.log('error1 => '+JSON.stringify(error));
                        });
                    },function(err){
                        console.log("open_err->  "+err);
                    });
                }        
                if(localStorage.getItem('device_type')=="ios"){
                    /*FilePicker.isAvailable(function (avail) {
                        console.log(avail ? "YES" : "NO");
                        FilePicker.pickFile(function(path) {
                          console.log("You picked this file: " + path);
                        },function(err) {
                          console.log("err: " + err);
                        });
                    });*/
                }
            }
        });
    }

    $scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj) {
        console.log("fileObj->>  "+JSON.stringify(fileObj));
    };

    /* Mobiscroll Timepicker functionality */

    $scope.settings = {
        theme: 'mobiscroll',
        //display: 'bottom',
        max: new Date(),
        dateFormat : "dd/mm/yy",
        timeFormat: 'hh:ii A',
        timeWheels: 'hhii A',
        animate: false,
        steps: { minute: 1, zeroBased: true }
    };
    
    /* Functionality for upload data */
    var letters = /^[A-Za-z ]+$/;
    $scope.uploadData = function() {
        if($scope.user.selCategory=="selCat"){
            customService.ToShowAlert('Please select category.');
            return;
        }else if($scope.user.providerName==""){
            customService.ToShowAlert('Please enter provider.');
            return;
        }else if(!letters.test($scope.user.providerName)){
            customService.ToShowAlert('Please enter valid provider.');
            return;
        }
        var dataFill = false;
        var correctData = false;
        for(var i=0; i<1; i++){
            if($scope.healthCondArr[i].name==""){               
                dataFill = false;
                break;
            }/*else if(!letters.test($scope.healthCondArr[i].name)){
                dataFill = true;
                correctData = false;
                break;
            }*/else{
                dataFill = true;
                correctData = true;
            }
        }
        if(dataFill == false){
            customService.ToShowAlert('Please enter health condition.');
            return;
        }else if(correctData==false){
            customService.ToShowAlert('Please enter valid health condition.');
            return;
        }else if($scope.user.title==""){
            customService.ToShowAlert('Please enter title.');
            return;
        }else if($scope.user.reportDate==undefined){
            customService.ToShowAlert('Please select date and time of report.');
            return;
        }else if(dataFill==true){   
            var valueArr = $scope.healthCondArr.map(function(item){ return item.name.toLowerCase() }); 
            var isDuplicate = valueArr.some(function(item, idx){ return valueArr.indexOf(item) != idx });
            if(isDuplicate==true){
                customService.ToShowAlert('There are same health conditions in the list. Please correct it.');
                return;
            }else{
                var dataFind = false;
                for(var i=0; i<$scope.providerArr.length; i++){
                    if($scope.user.providerName == $scope.providerArr[i].name){
                        $scope.user.providerId = $scope.providerArr[i].id;
                        dataFind = true;
                        break;
                    }else{
                        dataFind = false;
                    }
                }
                if(dataFind==false){
                    //customService.ToShowAlert('Provider name does not match with any record in database. Please first add to provider list.');
                    var msg = 'Provider name does not match with any record in database. Please first add the provider to provider list.';
                    navigator.notification.alert(msg,function(){
                        //customService.objUpload.category=$scope.user.selCategory;
                        customService.objUpload = {
                            category:$scope.user.selCategory,
                            providerName:$scope.user.providerName,
                            helCond:$scope.healthCondArr,
                            uploadPic:$scope.gridImageArr,
                            uploadDoc:$scope.docPathArr,
                            title:$scope.user.title,
                            time:new Date($scope.user.reportDate).getTime(),
                            notes:$scope.user.notes
                        }
                        //console.log("objUpload=>>   "+JSON.stringify(customService.objUpload));
                        customService.preStateForAddProvider="upload";
                        $state.go('addProvider');
                    },'Medical on Record','OK');
                }else{
                    $scope.user.helCondArr = [];
                    for(var i=0; i<$scope.healthCondArr.length; i++){
                        if($scope.healthCondArr[i].name!="")
                            $scope.user.helCondArr.push({name:$scope.healthCondArr[i].name,id:$scope.healthCondArr[i].id});
                    }
                    console.log("user_data->>  "+JSON.stringify($scope.user));
                    $scope.serviceObj = {
                        category: $scope.user.selCategory,
                        provider: $scope.user.providerName,
                        providerId: $scope.user.providerId,
                        title: $scope.user.title,
                        notes: $scope.user.notes,
                        time: new Date($scope.user.reportDate).getTime(),
                        uploadDoc:"",
                        uploadDocDir:""
                    };
                    var statement = "",healthCondId = "";                
                    for(var i=0; i<$scope.user.helCondArr.length; i++){
                        statement = statement + $scope.user.helCondArr[i].name+",";
                        healthCondId = healthCondId + $scope.user.helCondArr[i].id+",";
                    }
                    statement = statement.substring(0,statement.length-1);
                    healthCondId = healthCondId.substring(0,healthCondId.length-1);
                    $scope.serviceObj.helCond = statement;
                    $scope.serviceObj.helCondId = healthCondId;
                    $scope.serviceObj.uploadPic = "";
                    if($scope.gridImageArr.length!=0){
                        user.getBase64Image($scope.gridImageArr[count].img,saveImgToFile);                  
                    }else{
                        $scope.serviceObj.uploadPic = "";
                        console.log("serviceObj_data_else->>  "+JSON.stringify($scope.serviceObj));
                        $scope.saveDataInDB();
                    }
                }
            }             
        }
        
    }

    var afterImageStore = function(){
        var imageNameString=$scope.base64ImageArr[0];
        for(i=1;i<$scope.base64ImageArr.length;i++){
            imageNameString+=","+$scope.base64ImageArr[i];
        }
        $scope.serviceObj.uploadPic = imageNameString;
        //console.log("uploadPic: "+$scope.serviceObj.uploadPic);
        $scope.saveDataInDB();
    }

    /* Function for saving data into local DB */
    $scope.saveDataInDB = function(){
        console.log("saveDataInDB_funcCalled");
        //console.log("customService_uploadRecID->>  "+customService.uploadRecID);
        if($scope.docPathArr.length != 0){
            docs.copyPdf($scope.docPathArr,0)
            .then(function(docSucc){
                console.log('docSucc');
                var documentVal='';
                var documentDir=''; 
                for(var i=0;i<$scope.docPathArr.length;i++){
                    if(i == $scope.docPathArr.length-1){
                        documentVal=documentVal+$scope.docPathArr[i].oldFileName;
                        documentDir=documentDir+$scope.docPathArr[i].fileDirectory;
                        $scope.serviceObj.uploadDoc=documentVal;
                        $scope.serviceObj.uploadDocDir=documentDir;
                        saveWholeDate();
                    }else{
                        documentVal=documentVal+$scope.docPathArr[i].oldFileName+',';
                        documentDir=documentDir+$scope.docPathArr[i].fileDirectory+',';
                    }
                }
                console.log('documentVal => '+documentVal);
                console.log('documentDir => '+documentDir);
            },function(docErr){
                console.log('doc Error'+JSON.stringify(err));
            });
        }else{
            saveWholeDate();
        }       
    }
    
    function saveWholeDate(){
        console.log('serviceObj final values => '+JSON.stringify($scope.serviceObj));
        $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
        if(customService.uploadRecID!=""){
            var columnNameArr = ['category', 'provider', 'helthCond', 'uploadPic', 'uploadDoc', 'uploadDocDir', 'title', 'dateTime', 'notes', 'createdAt', 'modifiedAt','providerId','conditionIds','updtDate','sync']
            var columnValArr = [$scope.serviceObj.category,$scope.serviceObj.provider,$scope.serviceObj.helCond,$scope.serviceObj.uploadPic,$scope.serviceObj.uploadDoc,$scope.serviceObj.uploadDocDir,$scope.serviceObj.title,$scope.serviceObj.time,$scope.serviceObj.notes,$scope.serviceObj.time,$scope.serviceObj.time,$scope.serviceObj.providerId,$scope.serviceObj.helCondId,new Date().getTime(),"0",customService.uploadRecID]
            user.updateData('upload_tbl','uniId',columnNameArr,columnValArr).then(function(succ){
                //console.log("update upload_tbl succ->>  "+JSON.stringify(succ));
                $ionicLoading.hide();
                $state.go('reportPrescription');
            },function(err){
                $ionicLoading.hide();
                console.log("update upload_tbl err->>  "+JSON.stringify(err));
            });
        }else{
            console.log("else caled");
            user.uploadDataInDB(customService.userDBInfo.userId,$scope.serviceObj).then(function(objS){
                console.log("uploadDataInDB_succ->>  "+JSON.stringify(objS));
                $ionicLoading.hide();
                $state.go('reportPrescription');
            },function(objE){
                $ionicLoading.hide();
                console.log("uploadDataInDB_err->>  "+JSON.stringify(objE));
            });
        }
    }

    $scope.removeRecord = function(index){
        $scope.healthCondArr.splice(index, 1);
    }

    $rootScope.hwBackBtnPressed = function(){
        console.log("backBtnPressed_called->>>  ");
        $(".mbsc-fr-btn1").click();
        $scope.myGoBack();
    }
});
