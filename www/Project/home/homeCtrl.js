angular.module('PatientRecord.homeCtrl',[])
.controller('homeCtrl', function($scope, $state, customService, disease, DBInstance, localDB, global, user, $q, $rootScope, docs, $timeout, sharingSer, $ionicLoading) {
    $scope.$on("$ionicView.beforeEnter", function() {       
        $scope.showCarousel=false;
        $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
        $scope.obj={};
        $scope.ageYear = -5;
        fetchUserInfo();
        sharingSer.getAllSubProFromDB(-1)
        .then(function(objS){
            //console.log('All profile data => '+JSON.stringify(objS));
            var tempServData=[];
            for(var k=0;k<objS.length;k++){
                tempServData.push({server_userID:objS[k].server_userID,firstname:objS[k].firstname,middlename:objS[k].middlename,lastname:objS[k].lastname,img:objS[k].img});
            }
            var txtFile=[]
            for(var i=0;i<tempServData.length;i++){
               if(tempServData[i].img.search("base64") != -1){
               
               }else if(tempServData[i].img != ''){
                   txtFile.push({id:tempServData[i].server_userID,img:tempServData[i].img});
               }else{
                   tempServData[i].img='img/profile_pic.png';
               }
            }    
            //console.log('tempServData => '+JSON.stringify(tempServData));         
            docs.commonFuncImageFetch(txtFile,txtFile.length,0)
            .then(function(imgBaseSucc){
                for(var m=0;m<imgBaseSucc.length;m++){
                    var index=tempServData.findIndex(x => x.server_userID == imgBaseSucc[m].id);
                    tempServData[index].img = imgBaseSucc[m].img;
                }
                $scope.carouselArr=angular.copy(tempServData);
                $scope.obj.carCurrIndex=$scope.carouselArr.findIndex(x => x.server_userID==customService.carouselUserId);
                $scope.showCarousel=true;
                $ionicLoading.hide();
            },function(imgBaseErr){
                $ionicLoading.hide();
                console.log('objE => '+JSON.stringify(objE));
            })  
        },function(objE){
            //console.log('get all sub profile error => '+JSON.stringify(objE));
            $scope.showCarousel=true;
            $ionicLoading.hide();
        });
        
        disease.fetchMedication().then(function(objS){  
            var currTimeStamp = new Date().getTime();
            var dummyArr = [];
            for(var i=0; i<objS.length; i++){
                if(objS[i].curdDate<currTimeStamp){
                    dummyArr.push(objS[i].uniId);
                }
            }       
            console.log("dummyArr->>  "+JSON.stringify(dummyArr));   
            disease.updateMedication(dummyArr).then(function(succ){                
            },function(err){
            });
        },function(objE){
        });

        DBInstance.transaction(function(tx){
            var query="SELECT * FROM userReminders LEFT JOIN addMedication ON userReminders.medicationID = addMedication.uniId where addMedication.delStatus='active'";
            tx.executeSql(query, [], function(tx, results){
                var dbArr=[];
                for(var i=0;i<results.rows.length;i++){
                    dbArr.push(results.rows.item(i));
                }
                recFunc(dbArr);
            }, function(tx, err){

            });
            
            var query1="SELECT * FROM userReminders where reminderSource='vaccination'";
            tx.executeSql(query1, [], function(tx1, results1){
                var vaccineRemindArr=[];
                for(var r=0;r<results1.rows.length;r++){
                    vaccineRemindArr.push(results1.rows.item(r));
                }
                console.log('vaccineRemindArr -> '+JSON.stringify(vaccineRemindArr)); 
                recVaccineFunc(vaccineRemindArr);
            }, function(tx1, err1){

            });
        },function(err){
            console.log('addMedication db error');
        },function(){
            console.log('addMedication db succ');
        });
        
        var currTimeStamp = new Date().getTime();
        var currentInd = 0;
        function recFunc(total) {
            if (currentInd < total.length) {
                console.log('reminder start');
                    if(total[currentInd].curdDate < currTimeStamp){
                        cordova.plugins.notification.local.cancel(total.item(currentInd).reminderID, function () {
                            console.log('Notifications were cancelled');
                            localDB.deleteReminder(total[currentInd].uniId,total[currentInd].reminderSource).then(function(objS){ 
                                console.log('reminder deleted success -> '+objS);
                                currentInd++;
                                return recFunc(total);
                            },function(objE){
                                console.log("reminder deleted error ->>  "+objE);
                            });
                        });
                    }
            } else {
               console.log('reminder end');
               return;
            }
        }
        
        var currentInd1 = 0;
        function recVaccineFunc(total1) {
            //console.log('userId -> '+customService.userDBInfo.userId);
            if (currentInd1 < total1.length) {
                console.log("rec called111 "+total1[currentInd1].firstAt < currTimeStamp);
                if(total1[currentInd1].firstAt < currTimeStamp){
                    $q.all([user.updateUserVaccine('',total1[currentInd1].medicationID,customService.userDBInfo.userId),localDB.deleteReminder(total1[currentInd1].medicationID,'vaccination')]).then(function(objS){
                        console.log('vaccine updated -> '+objS);
                        console.log('reminder deleted success -> '+objS);
                    },function(objE){
                        console.log("reminder error ->>  "+objE);
                        console.log("reminder deleted error ->>  "+objE);
                    });
                }
                currentInd1++;
                return recVaccineFunc(total1);
            } else {
               console.log('reminder end');
               return;
            }
        }
    });
    
    function changeAge(){
        //console.log("changeAge_called");
        if($scope.userInfo.age!='') {         
            var ageDifMs = Date.now() - $scope.userInfo.age;
            var ageDate = new Date(ageDifMs);
            $scope.ageYear = Math.abs(ageDate.getUTCFullYear() - 1970);
            console.log("ageYear->>  "+$scope.ageYear);
        }else{
            $scope.ageYear = -5;
        }
    }
    
    $scope.$watch('obj.carCurrIndex', function (newIndex, oldIndex) {
        //console.log('oldIndex => '+oldIndex+'\n newIndex => '+newIndex);
        if(oldIndex != undefined){
            customService.carouselUserId=$scope.carouselArr[newIndex].server_userID;
            //console.log('carouselUserId => '+customService.carouselUserId);
            fetchUserInfo();
            $scope.$emit('carouselChange', {message: "User change the carousel"});
        }
    });

    function fetchUserInfo(){
        user.fetchUserInfo().then(function(objS){
            customService.userDBInfo = objS;
            //console.log('userDBInfo => '+JSON.stringify(customService.userDBInfo));
            $scope.userInfo = customService.userDBInfo;
            if($scope.userInfo.bloodgrp=='')
                $scope.userInfo.bloodgrp = 'Not Known';
            changeAge();
            fetchuserDiseaseCount();               
        },function(objE){
            $ionicLoading.hide();
            //console.log('app_fetchUserInfo_err:-- '+JSON.stringify(objE));
        }); 
    }

    function fetchuserDiseaseCount(){
        $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
        var allCategory = [
            user.fetchProviderList(['Doctor','Lab','Pharmacy']),
            disease.fetchUserPreexistCondn(),
            disease.getMedicineList(),
            disease.fetchUserAllergy(),
            user.getUserVaccines(customService.userDBInfo.userId),
            docs.fetchDoc(),
            disease.fetchBloodPressure(),
            disease.fetchBloodSugar(),
            disease.fetchEyePower(),
            disease.fetchTemperature(),
            disease.fetchOtherVS(),
            user.fetchEmergencyContact(),
            sharingSer.getAllSubProFromDB(localStorage.getItem('loginUserId')),
            user.fetchUploadData("SELECT * FROM upload_tbl where server_userID="+customService.carouselUserId+" AND delStatus='active'"),
            user.fetchVaccineRemind(customService.userDBInfo.userId)
        ]
        $q.all(allCategory)
        .then(function(countSucc){
            $scope.providerDet={count:countSucc[0].length};
            $scope.healCondnDet={count:countSucc[1].length, detail:maxValIndexForHel(countSucc[1])};
            $scope.medDet={count:countSucc[2].length, detail:maxValIndexForMed(countSucc[2])};
            $scope.allergyDet={count:countSucc[3].length, detail:maxValIndexForAle(countSucc[3])};
            //$scope.vaccDocDet={count:(countSucc[4].length+countSucc[5].length), detail:maxValIndex(countSucc[4].concat(countSucc[5]))};            
            $scope.vitalSymDet={count:(countSucc[6].length+countSucc[7].length+countSucc[8].length+countSucc[9].length+countSucc[10].length), detail:maxValIndexForVS(countSucc[6],countSucc[7],countSucc[8],countSucc[9],countSucc[10])};
            if(countSucc[11].contact1 == 'rel1' && countSucc[11].contact2 == 'rel2'){
                $scope.emerDet={count:0, detail:{}};
            }else if(countSucc[11].contact1 != 'rel1' && countSucc[11].contact2 != 'rel2'){
                $scope.emerDet={count:2};
                $scope.emerDet.detail={name:countSucc[11].fatherName,type:countSucc[11].contact1};
            }else if(countSucc[11].contact1 != 'rel1' || countSucc[11].contact2 != 'rel2'){
                $scope.emerDet={count:1};
                if(countSucc[11].contact1 != 'rel1')
                    $scope.emerDet.detail={name:countSucc[11].fatherName,type:countSucc[11].contact1};
                else if(countSucc[11].contact2 != 'rel2')
                    $scope.emerDet.detail={name:countSucc[11].motherName,type:countSucc[11].contact2};
            }
            //console.log('emerDet => '+JSON.stringify($scope.emerDet));

            $scope.famFrndDet={count:countSucc[12].length};
            $scope.rpsDet={count:countSucc[13].length, detail:maxValIndexForRSP(countSucc[13])};
            

            /* code for Vaccine and Doc */
            var userVaccineArr = countSucc[4];
            var vaccineRemindTbl = countSucc[14];            
            for(var i=0; i<userVaccineArr.length; i++){
                var index=vaccineRemindTbl.findIndex(x => x.vaccineID==userVaccineArr[i].vaccineID);
                console.log("index->  "+index);
                if(index != -1){
                    userVaccineArr[i].remDate=vaccineRemindTbl[index].vaccineReminder;
                }else{
                    userVaccineArr[i].remDate=""; 
                }
            }
            $scope.vaccDocDet={count:(countSucc[4].length+countSucc[5].length), detail:getData(userVaccineArr)};
            console.log("vaccDocDet=>>  "+JSON.stringify($scope.vaccDocDet));
            $ionicLoading.hide();
        },function(countErr){
            $ionicLoading.hide();
            console.log("all error=>>  "+JSON.stringify(countErr)); 
        })
    }

    /*function maxValIndex(arr){
        var maxObj = _.max(arr, function (obj) {
          return obj.initDate;
        });
        var maxIndex = arr.indexOf(maxObj);
        return arr[maxIndex];
    }*/

    function getData(arr){
        var completeVD = [];
        for(var i=0; i<arr.length; i++){
            if(arr[i].remDate!="")
                completeVD.push(arr[i]);
        }
        completeVD = completeVD.sort(function(dataA, dataB) {
            if (dataA.remDate < dataB.remDate) {
                return -1;
            } else if (dataA.remDate > dataB.remDate) {
                return 1;
            } else {
                return 0;
            }
        });
        if(completeVD.length==0)
            return completeVD;
        else
            return completeVD[0];
    }

    function maxValIndexForHel(arr){
        var maxObj = _.max(arr, function (obj) {
          return obj.findingDate;
        });
        var maxIndex = arr.indexOf(maxObj);
        return arr[maxIndex];
    }

    function maxValIndexForMed(arr){
        var maxObj = _.max(arr, function (obj) {
          return obj.start_date;
        });
        var maxIndex = arr.indexOf(maxObj);
        return arr[maxIndex];
    }

    function maxValIndexForAle(arr){
        var maxObj = _.max(arr, function (obj) {
          return obj.findingDate;
        });
        var maxIndex = arr.indexOf(maxObj);
        return arr[maxIndex];
    }

    function maxValIndexForRSP(arr){
        var maxObj = _.max(arr, function (obj) {
          return obj.dateTime;
        });
        var maxIndex = arr.indexOf(maxObj);
        return arr[maxIndex];
    }
        
    function maxValIndexForVS(bpArr,bsArr,eyeArr,tempArr,otherArr){
        console.log("maxValIndexForVS called");
        var completeVital = [];
        completeVital = completeVital.concat(bpArr).concat(bsArr).concat(eyeArr).concat(tempArr).concat(otherArr);
        completeVital = completeVital.sort(function(dataA, dataB) {
            if (dataA.modifiedAt > dataB.modifiedAt) {
                return -1;
            } else if (dataA.modifiedAt < dataB.modifiedAt) {
                return 1;
            } else {
                return 0;
            }
        });
        //console.log("completeVital=>  "+JSON.stringify(completeVital));
        if(completeVital.length==0)
            return completeVital;
        else
            return completeVital[0];
    }

    $scope.timeline = function() {
        global.timelineId="";
        $state.go('timeline');
    }
    
    $scope.sahre = function() {
        customService.stateForShare = "menu.home";
        $state.go('share');
    }
        
    $scope.upload = function() {
        customService.uploadRecID = "";
        customService.typeOfUpload = "";
        $state.go('upload');
    }
            
    $scope.personalInfo = function() {
        //customService.ToShowAlert("Work in progress.");
        $state.go('personal');
    }
            
    $scope.myProvider = function() {
        //customService.ToShowAlert("Work in progress.");
        $state.go('provider');
    }
            
    $scope.healthCond = function() {
        customService.objectForShare=[];
        $state.go('healthConditions');
    }
            
    $scope.medication = function() {
        //customService.ToShowAlert("Work in progress.");
        customService.objectForShare=[];
        $state.go('medication');
    }
            
    $scope.allergyDis = function() {
        customService.objectForShare=[];
        $state.go('allergyPatient');
    }
            
    $scope.vaccineDocument = function() {
        //customService.ToShowAlert("Work in progress.");
        customService.objectForShare=[];
        $state.go('vaccineDocument');
    }
            
    $scope.vitalSymptoms = function() {
        //customService.ToShowAlert("Work in progress.");
        customService.objectForShare=[];
        global.vitalId = "";
        $state.go('vitalSymptoms');
    }
            
    $scope.emergencyContact = function() {
        //customService.ToShowAlert("Work in progress.");
        $state.go('emergencyContactView');
    }
            
    $scope.familyFriend = function() {
        //customService.ToShowAlert("Work in progress.");
        customService.fandFActiveTab = "mngLinkTab";
        $state.go('familyFriend');
    }
            
    $scope.reportPrescription = function() {
        //customService.ToShowAlert("Work in progress.");
        customService.objRSP={};
        $state.go('reportPrescription');
    }
    
    $rootScope.hwBackBtnPressed = function(e){
        console.log("backBtnPressed_called->>>  ");
        $(".mbsc-fr-btn1").click();
        //$scope.myGoBack();
        if($scope.backButtonPressedOnceToExit){
            navigator.app.exitApp();
        }else {
            $scope.backButtonPressedOnceToExit = true;
            window.plugins.toast.showShortBottom("Press back button again to exit",function(a){},function(b){});
            setTimeout(function() {
                $scope.backButtonPressedOnceToExit = false;
            }, 2000);
        }        
        e.preventDefault();
        return false;
    }
    
    $scope.$on("$ionicView.leave", function() {
        customService.objectForShare = [];
        console.log("done => "+JSON.stringify(customService.objectForShare));
    })
});
