angular.module('PatientRecord.loginCtrl',[])
.controller('loginCtrl', function($scope, $state, customService, $ionicPopover, user, $ionicLoading, disease, $rootScope, sharingSer, $q, docs, $cordovaDevice, doctor, localDB, sync) {

    $scope.$on("$ionicView.beforeEnter", function() {
        $scope.user = {}
        $scope.user.mobile = '';
        $scope.user.loginVali = true;
        $scope.user.passVali = true;
        $scope.currImage = "img/LoginUser.png";
        if(localStorage.getItem('rememberMe')=='Yes') {
            $scope.user.mobile = localStorage.getItem('mobileNo');
        } else {
            $scope.user.mobile = '';
        }
        //$scope.user.mobile = '9716682259';
        //$scope.user.password = 'Vishal1@';
        $scope.user.password = '';
        if(localStorage.getItem('rememberMe')=='Yes') {
            $scope.rmbrMe = true;
        } else {
            $scope.rmbrMe = false;
        }
        if(customService.currentLoginUser == 'user') {
            $scope.header = 'Login as User';
            $scope.loginPop = 'Login as Doctor';
            $scope.signupPop = 'Sign Up as Doctor';
            $scope.currImage = "img/LoginUser.png";
            $scope.currRole = 'SIGN IN';
        } else {
            $scope.header = 'Login as Doctor';
            $scope.loginPop = 'Login as User';
            $scope.signupPop = 'Sign Up as User';
            $scope.currImage = "img/sign_right_icn.png";
            $scope.currRole = 'SIGN IN AS DOCTOR';
        }

        $scope.sliderArr = [{img:"img/slider.png"},{img:"img/slider.png"}];
    });
     
    $(document).on('keydown','#mobileNo',function(e) {
        if(e.which=='13') {
            $('#paswrd').focus();
        }
    })
    
    $(document).on('keydown','#paswrd',function(e) {
        if(e.which=='13') {
            $('#paswrd').blur();
            //$scope.login();
        }
    })

    $scope.rembrMe = function() {
        if($scope.rmbrMe == false) {
            $scope.rmbrMe = true;
        } else {
            $scope.rmbrMe = false;
        }
    }
            
    $scope.login = function() {
        var PasswordRegex = RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        var numberRegx = (/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);
        if ($scope.user.mobile == '') {
            customService.ToShowAlert('Please enter mobile number.');
            $scope.user.loginVali = false;
            return;
        } else if (!numberRegx.test($scope.user.mobile)) {
            customService.ToShowAlert('Please enter valid mobile number.');
            $scope.user.loginVali = false;
            return;
        }
        $scope.user.loginVali = true;
        if($scope.user.password=='') {
            customService.ToShowAlert('Please enter password.');
            $scope.user.passVali = false;
            return;
        } else if(!PasswordRegex.test($scope.user.password)) {
            customService.ToShowAlert('Please enter valid password.');
            $scope.user.passVali = false;
            return;
        }
        $scope.user.passVali = true;
        var loginData = {
            "mobile": $scope.user.mobile,
            "password": $scope.user.password,
            "device_token":"1234567890",
            "device_type":'android'
        }
            
        console.log('loginData:- '+JSON.stringify(loginData));
        $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});        
        if(customService.connection==true){
            user.postMethod(loginData, 'signin').then(function(objS) {
                //console.log("login_succ=>>  "+JSON.stringify(objS));
                if (objS.responseCode == '200') {
                    if(objS.responseData.user_details.status=="1" || objS.responseData.user_details.status==1){
                        localDB.deleteDB().then(function(){
                            user.userDetail(objS.responseData.user_details.id).then(function(syncSucc){
                                customService.carouselUserId=objS.responseData.user_details.id;
                                console.log('login id => '+customService.carouselUserId);
                                //sharingSer.fetchRelationForShare(objS.responseData.user_details.id);
                                //sharingSer.fetchContact($cordovaDevice.getUUID(),objS.responseData.user_details.id);
                                disease.insertSettingData(objS.responseData.user_details.id,objS.responseData.user_settings);
                                customService.userId = objS.responseData.user_details.id;
                                localStorage.setItem('loginUserId',objS.responseData.user_details.id)
                                localStorage.setItem('userMobileNo',$scope.user.mobile)
                                /*if($scope.rmbrMe==true) {
                                    localStorage.setItem('rememberMe','Yes')
                                    localStorage.setItem('mobileNo',$scope.user.mobile)
                                } else {
                                    localStorage.setItem('rememberMe','No')
                                    localStorage.setItem('mobileNo','')
                                }*/
                                localStorage.setItem('rememberMe','Yes');
                                localStorage.setItem('mobileNo',$scope.user.mobile);
                                localStorage.setItem('loginStatus','Yes');

                                if(syncSucc.data.responseData.role_id == "3"){
                                    var parallelArr=[user.fetchUserInfo(),sharingSer.fetchSubProfile(localStorage.getItem('loginUserId'))];
                                    localStorage.setItem('loogedInUserDet',JSON.stringify({mobile:loginData.mobile,pwd:loginData.password,id:customService.carouselUserId}));
                                }
                                else if(syncSucc.data.responseData.role_id == "2"){
                                    var parallelArr=[user.fetchUserInfo()];
                                }
                                $q.all(parallelArr)
                                .then(function(userInfoSucc){
                                    customService.userDBInfo = userInfoSucc[0];
                                    //console.log("userDBInfo=>> "+JSON.stringify(customService.userDBInfo));
                                    sync.syncDepData();
                                    if(customService.userDBInfo.role == "2"){
                                        sync.synOnlyDoc();
                                        doctor.checkDrApprovedByAdmin(customService.userDBInfo.userId).then(function(drSucc){
                                            //console.log('drSucc=>> '+JSON.stringify(drSucc));
                                            if(drSucc.data.responseCode==200){
                                                var obj={
                                                    id:customService.userDBInfo.userId,
                                                    name:customService.userDBInfo.fullName,
                                                    email:customService.userDBInfo.email,
                                                    pic:customService.userDBInfo.image,
                                                    mobile:customService.userDBInfo.mobile
                                                }
                                                if(drSucc.data.responseData.profile_verified==1 || drSucc.data.responseData.profile_verified=="1"){
                                                    obj.verify_status="APPROVED";
                                                    localStorage.setItem('loogedInUserDet',JSON.stringify({mobile:loginData.mobile,pwd:loginData.password,id:customService.carouselUserId}));
                                                }else
                                                    obj.verify_status="PENDING";
                                                doctor.insertDoctorDet(obj).then(function(objSucc){
                                                    console.log('inserted success');
                                                    doctor.getDoctorDet(customService.userDBInfo.userId).then(function(docDetSucc){   
                                                        var proObj = {
                                                            id:customService.userDBInfo.userId,
                                                            fName:customService.userDBInfo.firstname,
                                                            mName:customService.userDBInfo.middlename,
                                                            lName:customService.userDBInfo.lastname,
                                                            mobile:customService.userDBInfo.mobile,
                                                            email:customService.userDBInfo.email,
                                                        }
                                                        doctor.instDrAsProvider(proObj).then(function(succ){
                                                            localStorage.setItem('proIDForDr',succ);
                                                            if(docDetSucc.verify_status == "PENDING"){
                                                                $ionicLoading.hide();
                                                                customService.docProBackBtn = false;
                                                                $state.go('dr_viewProfileAfterSignup');
                                                            }else{
                                                                doctor.insertPatientList(customService.userDBInfo.userId)
                                                                .then(function(insSucc){
                                                                    $ionicLoading.hide();
                                                                    $state.go('dr_menu.dr_patientList');
                                                                },function(insErr){
                                                                    $ionicLoading.hide();
                                                                });
                                                            }
                                                        },function(err){
                                                            $ionicLoading.hide();
                                                            console.log("instDrAsProvider_err");
                                                        });                                                
                                                    },function(docDetErr){
                                                        $ionicLoading.hide();
                                                        console.log("getDoctorDet_err");
                                                    });
                                                },function(objErr){
                                                    $ionicLoading.hide();
                                                    console.log('inserted error');
                                                });
                                            }else{
                                                console.log("status_err");
                                                $ionicLoading.hide();
                                            }                                            
                                        },function(drErr){
                                            $ionicLoading.hide();
                                            console.log('checkDrApprovedByAdmin error');
                                        });                                        
                                    }else if(customService.userDBInfo.role == "3"){
                                        sync.synOnlyPat();
                                        sync.phoneContactSendToServer();
                                        if(userInfoSucc[1].data.responseCode==200){
                                            var subProfList=userInfoSucc[1].data.responseData;
                                            sharingSer.insSettingForSubProfile(subProfList,localStorage.getItem('loginUserId'))
                                            .then(function(setSucc){},function(setErr){});
                                            var imgArr=[];
                                            for(var m=0;m<subProfList.length;m++){
                                                subProfList[m].imgTxt="";
                                                if(subProfList[m].image != "")
                                                    imgArr.push({id:subProfList[m].id,img:subProfList[m].image});
                                            }
                                            docs.commonFuncImageStore(imgArr).then(function(imgSucc){
                                                //console.log('imgSucc => '+JSON.stringify(imgSucc));
                                                for(var n=0;n<imgSucc.length;n++){
                                                    var index=subProfList.findIndex(x => x.id == imgSucc[n].id);
                                                    subProfList[index].imgTxt = imgSucc[n].img;
                                                }
                                                //console.log('subProfList => '+JSON.stringify(subProfList));
                                                sharingSer.insertSubProfile(subProfList,localStorage.getItem('loginUserId')).then(function(insSubProfSuc){
                                                    console.log("in_sub_pro_suc->>  "+JSON.stringify(insSubProfSuc));
                                                    $ionicLoading.hide();
                                                    $state.go('menu.home');
                                                },function(insSubProfEr){
                                                    $ionicLoading.hide();
                                                    console.log("in_sub_pro_err->>  "+JSON.stringify(insSubProfEr));
                                                    //$state.go('menu.home');
                                                });
                                            },function(imgErr){
                                                console.log('imgErr => '+JSON.stringify(imgErr));
                                            })
                                        }else{
                                            $ionicLoading.hide();
                                            //$state.go('menu.home');
                                            console.log("all_err=>> "+JSON.stringify(userInfoSucc[1]));
                                            customService.ToShowAlert(userInfoSucc[1].data.responseMessage);
                                        }
                                    } 
                                },function(userInfoErr){
                                    $ionicLoading.hide();
                                    //$state.go('menu.home');
                                    console.log('userInfoErr -> '+JSON.stringify(userInfoErr));
                                });
                            },function(syncErr){
                                $ionicLoading.hide();
                                console.log('error -> '+JSON.stringify(syncErr));
                            })
                        },function(){
                            $ionicLoading.hide();
                        });
                    }else{
                        $ionicLoading.hide();
                        navigator.notification.alert("Your otp verification is still pending. Please first do it.",function(){
                            customService.userId = objS.responseData.user_details.id;
                            customService.signupPwd = $scope.user.password;
                            localStorage.setItem('loginUserId',objS.responseData.user_details.id);
                            localStorage.setItem('userMobileNo',$scope.user.mobile)
                            customService.carouselUserId=parseInt(localStorage.getItem('loginUserId'));
                            customService.OTPCode = objS.responseData.user_details.otp.split('');
                            $state.go('OTPverify');
                        },'Medical on Record','OK');
                    }
                }
            }, function(objE) {
                console.log("signin_err->>  "+JSON.stringify(objE));
                $ionicLoading.hide();
                if(objE.msg.responseCode==400){
                    customService.ToShowAlert(objE.msg.responseMessage);
                } else {
                    customService.ToShowAlert('Network error.');
                }
            })
        }else{
            var Detail=JSON.parse(localStorage.getItem('loogedInUserDet'));
            console.log('loogedInUserDet => '+localStorage.getItem('loogedInUserDet'));
            if(Detail == null){
                $ionicLoading.hide();
                window.plugins.toast.showShortBottom("Please check your internet connection.",function(a){},function(b){});
            }else if(Detail.mobile == loginData.mobile){
                console.log('loginData => '+JSON.stringify(loginData));
                if(Detail.pwd == loginData.password){
                    customService.carouselUserId=Detail.id;
                    customService.userId = Detail.id;
                    localStorage.setItem('loginUserId',Detail.id)
                    localStorage.setItem('userMobileNo',loginData.mobile)
                    /*if($scope.rmbrMe==true) {
                        localStorage.setItem('rememberMe','Yes')
                        localStorage.setItem('mobileNo',loginData.mobile)
                    } else {
                        localStorage.setItem('rememberMe','No')
                        localStorage.setItem('mobileNo','')
                    }*/
                    localStorage.setItem('rememberMe','Yes');
                    localStorage.setItem('mobileNo',$scope.user.mobile);
                    localStorage.setItem('loginStatus','Yes');
                    user.fetchUserInfo().then(function(objS){
                        customService.userDBInfo = objS; 
                        if(customService.userDBInfo.role == "3"){
                            $ionicLoading.hide();                                
                            $state.go('menu.home');
                        }else if(customService.userDBInfo.role == "2"){
                            doctor.getDoctorDet(customService.userDBInfo.userId)
                            .then(function(docDetSucc){
                                $ionicLoading.hide();
                                if(docDetSucc.verify_status == "PENDING")
                                    $state.go('dr_viewProfileAfterSignup');
                                else
                                    $state.go('dr_menu.dr_patientList');
                            },function(docDetErr){
                                $ionicLoading.hide();
                            })
                        }  
                    },function(){
                        $ionicLoading.hide();
                    })
                }else{
                    $ionicLoading.hide();
                    customService.ToShowAlert('Password does not match.');
                }
            }else{
                $ionicLoading.hide();
                window.plugins.toast.showShortBottom("Please check your internet connection.",function(a){},function(b){});
            }
        }
    }
    
    $scope.forgetPass = function() {
        $state.go('forget');
        //window.plugins.nativepagetransitions.slide({"direction":"left"},function (msg) {},function (msg) {});
    }
            
    $scope.signup = function() {
        //$rootScope.previousState = "login";
        $state.go('signup');
        //window.plugins.nativepagetransitions.slide({"direction":"left"},function (msg) {},function (msg) {});
    }
            
    $ionicPopover.fromTemplateUrl('popover.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
    });
            
    $scope.openPopover = function($event) {
        if(customService.currentLoginUser == 'user') {
            $scope.loginPop = 'Login as Doctor';
            $scope.signupPop = 'Sign Up as Doctor';
        } else {
            $scope.loginPop = 'Login as User';
            $scope.signupPop = 'Sign Up as User';
        }
        $scope.popover.show($event);
    };
            
    $scope.closePopover = function() {
        $scope.popover.hide();
    };
            
    $scope.loginDocUser = function() {
        if(customService.currentLoginUser == 'user') {
            $scope.header = 'Login as Doctor';
            $scope.user.mobile = '';
            $scope.user.password = '';
            customService.currentLoginUser = 'doctor';
            $scope.currImage = "img/sign_right_icn.png";
            $scope.currRole = 'SIGN IN AS DOCTOR';
        } else {
            $scope.header = 'Login as User';
            $scope.user.mobile = '';
            $scope.user.password = '';
            customService.currentLoginUser = 'user';
            $scope.currImage = "img/LoginUser.png";
            $scope.currRole = 'SIGN IN';
        }
        $scope.closePopover();
    }
            
    $scope.signUpDocUser = function() {
        if(customService.currentLoginUser == 'user') {
            customService.currentLoginUser = 'doctor';
            $scope.currRole = 'SIGN IN AS DOCTOR';
        } else {
            customService.currentLoginUser = 'user';
            $scope.currRole = 'SIGN IN';
        }
        $scope.closePopover();
        $state.go('signup');
        //$state.go('dr_editProfile');
    }
            
    $rootScope.hwBackBtnPressed = function(){
        console.log("backBtnPressed_called->>>  ");
        $(".mbsc-fr-btn1").click();
        if($scope.backButtonPressedOnceToExit){
            navigator.app.exitApp();
        }else {
            $scope.backButtonPressedOnceToExit = true;
            window.plugins.toast.showShortBottom("Press back button again to exit",function(a){},function(b){});
            setTimeout(function() {
                $scope.backButtonPressedOnceToExit = false;
            }, 2000);
        }        
        e.preventDefault();
        return false;
    }
});
