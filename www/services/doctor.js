angular.module('PatientRecord.doctor', [])
.service('doctor',function($q, $http, baseUrl, user, disease, DBInstance, customService) {
    var self = this;
    
    self.insertDoctorDet=function(obj){
        //console.log('obj ----> '+JSON.stringify(obj));
        var deff=$q.defer();
        DBInstance.transaction(function(tx){
            tx.executeSql("insert into DoctorDetail (server_userID, img, name, email, mobile, verify_status, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?)",[obj.id,obj.pic,obj.name,obj.email,obj.mobile,obj.verify_status,new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                console.log('user inserted');
           });
        },function(err){
            console.log('doctor detail inserted error -> ');
            deff.reject(err);
        },function(succ){
            console.log('doctor detail inserted ->');
            deff.resolve(succ);
        });   
        return deff.promise;
    } 
    
    self.getDoctorDet=function(doctor_id){
        var deff=$q.defer();
        var docData='';
        DBInstance.transaction(function(tx){
            tx.executeSql("select * from DoctorDetail where server_userID=?",[doctor_id],function(tx,res){
                //console.log('user fetched => '+JSON.stringify(res.rows));
                if(res.rows.length>0)
                    docData=res.rows.item(0);
           });
        },function(err){
            console.log('doctor detail fetched error -> ');
            deff.reject(err);
        },function(succ){
            console.log('doctor detail fetched ->');
            deff.resolve(docData);
        });   
        return deff.promise;
    }

/***** Fetch provider for checking the provider that are added *****/
    self.getAllProvider = function(){
        var deff=$q.defer();
        var allProArr = [];
        DBInstance.transaction(function(tx){
            var query = "select * from userProviders";
            tx.executeSql(query,[],function(tx,res){
                for(var i=0; i<res.rows.length;i++){
                    allProArr.push(res.rows.item(i));
                }
                //console.log("allProArr=> "+JSON.stringify(allProArr));     
            });
        },function(err){
            console.log('userProviders db error');
            deff.reject(err);
        },function(succ){
            console.log('userProviders db succ');
            deff.resolve(allProArr);
        });
        return deff.promise;
    }

    
/***********insert user providers into local db************/        
    self.insertUserProvider=function(user_id,providerArr){
        var deff=$q.defer();
        var idArr='';
        self.getAllProvider().then(function(objS){
            if(objS.length!=0){
                if(providerArr.length!=0){
                    var currInd = 0;
                    var total = providerArr.length;
                    insert(currInd,total);
                    function insert(currInd,total){
                        console.log("recursion_called");
                        if(currInd<total){
                            var ind = objS.findIndex(x => x.mobile == providerArr[currInd].mobile);
                            var arr = [parseInt(localStorage.getItem('loginUserId')),providerArr[currInd].name,'','',providerArr[currInd].type,providerArr[currInd].mobile,providerArr[currInd].email,'Not Verify',new Date().getTime(),new Date().getTime(),new Date().getTime(),new Date().getTime(),new Date().getTime()];
                            var arr1 = [user_id,providerArr[currInd].name,'','',providerArr[currInd].type,providerArr[currInd].mobile,providerArr[currInd].email,'Not Verify',new Date().getTime(),new Date().getTime(),new Date().getTime(),new Date().getTime(),new Date().getTime()];
                            if(ind==-1){
                                DBInstance.transaction(function(tx){
                                    tx.executeSql("insert into userProviders (server_ProviderID, firstname, middlename, lastname, role, mobile, email, status, createdAt, modifiedAt, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?)",arr,function(txScr,resScr){
                                        console.log('userProviders inserted for dr');
                                        idArr=idArr+','+resScr.insertId;
                                        tx.executeSql("insert into userProviders (server_ProviderID, firstname, middlename, lastname, role, mobile, email, status, createdAt, modifiedAt, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?)",arr1,function(txScr,resScr){
                                            console.log('userProviders inserted for pat');
                                        });
                                    });
                                },function(err){
                                    console.log(err);
                                    deff.reject(err);
                                },function(succ){
                                    console.log('success for dr');
                                    //deff.resolve(idArr);
                                    currInd++;
                                    insert(currInd,total);
                                });                      
                            }else{
                                idArr=idArr+','+objS[ind].providerId;
                                var patInd = objS.findIndex(x => x.server_ProviderID.toString() == user_id.toString());
                                if(patInd==-1){
                                    DBInstance.transaction(function(tx){
                                        tx.executeSql("insert into userProviders (server_ProviderID, firstname, middlename, lastname, role, mobile, email, status, createdAt, modifiedAt, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?)",arr1,function(txScr,resScr){
                                            console.log('userProviders inserted for pat');
                                        });
                                    },function(err){
                                        console.log(err);
                                        deff.reject(err);
                                    },function(succ){
                                        console.log('success for dr');
                                        //deff.resolve(idArr);
                                        currInd++;
                                        insert(currInd,total);
                                    });
                                }else{
                                    currInd++;
                                    insert(currInd,total);
                                }
                            }
                        }else{
                            console.log("recursion_complete");
                            deff.resolve(idArr);
                        }
                    }
                }else
                    deff.resolve(idArr);    
            }else{
                DBInstance.transaction(function(tx){      
                    for(var i=0;i<providerArr.length;i++){
                        tx.executeSql("insert into userProviders (server_ProviderID, firstname, middlename, lastname, role, mobile, email, status, createdAt, modifiedAt, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?)",[parseInt(localStorage.getItem('loginUserId')),providerArr[i].name,'','',providerArr[i].type,providerArr[i].mobile,providerArr[i].email,'Not Verify',new Date().getTime(),new Date().getTime(),new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                            console.log('userProviders inserted');
                            idArr=idArr+','+resScr.insertId;
                        });
                        if(i==providerArr.length-1)
                            self.insertProviderForPatient(user_id,providerArr);
                    }      
                },function(err){
                    console.log(err);
                    deff.reject(err);
                },function(succ){
                    console.log('success');
                    deff.resolve(idArr);
                });
                //return deff.promise;
            }
        },function(objE){
            console.log("getAllProvider_err");
            deff.reject(objE);
        });
        return deff.promise;
    }

/***********insert providers into local db for patient ************/        
    self.insertProviderForPatient = function(user_id,providerArr){
        console.log("insertProviderForPatient_called");
        var deff=$q.defer();
        //var idArr='';
        DBInstance.transaction(function(tx){      
            for(var i=0;i<providerArr.length;i++){
                tx.executeSql("insert into userProviders (server_ProviderID, firstname, middlename, lastname, role, mobile, email, status, createdAt, modifiedAt, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?)",[user_id,providerArr[i].name,'','',providerArr[i].type,providerArr[i].mobile,providerArr[i].email,'Not Verify',new Date().getTime(),new Date().getTime(),new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                    console.log('userProviders inserted');
                    //idArr=idArr+','+resScr.insertId;
                });
            }      
        },function(err){
            console.log("pat pro ins err-> "+err);
            deff.reject(err);
        },function(succ){
            console.log('pat pro ins succ-> '+ succ);
            deff.resolve();
        });
        return deff.promise;
    }

/***********insertion for add vitals ******************/        
    self.insertTempDR=function(user_id,fVal,cVal,recTime){
        var deff=$q.defer();
        var idArr='';
        DBInstance.transaction(function(tx){      
            tx.executeSql("INSERT INTO VS_temperature (server_userID, fValue, cValue, recordDateTime, notes, modifiedAt, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?)",[user_id,fVal,cVal,recTime,"",recTime,new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                    console.log('insertTempDR inserted=>  ');
                    idArr=idArr+','+resScr.insertId;
            });      
        },function(err){
            console.log(err);
            deff.reject(err);
        },function(succ){
            console.log('success');
            deff.resolve(idArr);
        });
        return deff.promise;
    }

    self.insertHeartRateDR=function(user_id,recTime,val){
        var deff=$q.defer();
        var idArr='';
        DBInstance.transaction(function(tx){      
            tx.executeSql("INSERT INTO heartRateDB (server_userID, recTime, value, initDate, updtDate, local_id) values (?,?,?,?,?,?)",[user_id,recTime,val,new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                    console.log('insertHeartRateDR inserted=>  ');
                    idArr=idArr+','+resScr.insertId;
            });      
        },function(err){
            console.log(err);
            deff.reject(err);
        },function(succ){
            console.log('success');
            deff.resolve(idArr);
        });
        return deff.promise;
    }

    self.insertRespRateDR=function(user_id,recTime,val){
        var deff=$q.defer();
        var idArr='';
        DBInstance.transaction(function(tx){      
            tx.executeSql("INSERT INTO respRateDB (server_userID, recTime, value, initDate, updtDate, local_id) values (?,?,?,?,?,?)",[user_id,recTime,val,new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                    console.log('insertRespRateDR inserted=>  ');
                    idArr=idArr+','+resScr.insertId;
            });      
        },function(err){
            console.log(err);
            deff.reject(err);
        },function(succ){
            console.log('success');
            deff.resolve(idArr);
        });
        return deff.promise;
    }

    self.insertBPDR=function(user_id,sVal,dVal,recTime){
        var deff=$q.defer();
        var idArr='';
        DBInstance.transaction(function(tx){      
            tx.executeSql("INSERT INTO VS_bloodPressure (server_userID,systolicVal, diastolicVal, recordDateTime, notes, modifiedAt, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?)",[user_id,sVal,dVal,recTime,"",recTime,new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                    console.log('insertBPDR inserted=>  ');
                    idArr=idArr+','+resScr.insertId;
            });      
        },function(err){
            console.log(err);
            deff.reject(err);
        },function(succ){
            console.log('success');
            deff.resolve(idArr);
        });
        return deff.promise;
    }

    self.insertBSDR=function(user_id,sVal,type,recTime){
        var deff=$q.defer();
        var idArr='';
        DBInstance.transaction(function(tx){      
            tx.executeSql("INSERT INTO VS_bloodSugar (server_userID, sugarVal, mealType, recordDateTime, notes, modifiedAt, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?)",[user_id,sVal,type,recTime,"",recTime,new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                    console.log('insertBSDR inserted=>  ');
                    idArr=idArr+','+resScr.insertId;
            });      
        },function(err){
            console.log(err);
            deff.reject(err);
        },function(succ){
            console.log('success');
            deff.resolve(idArr);
        });
        return deff.promise;
    }

    self.insertWeightDR=function(user_id,kgVal,lbsVal,recTime){
        var deff=$q.defer();
        var idArr='';
        DBInstance.transaction(function(tx){      
            tx.executeSql("insert into userWeight (server_userID, kgVal, lbsVal, recordDate, notes, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?)",[user_id,kgVal,lbsVal,recTime,"",new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                console.log('insertWeightDR inserted=>  ');
                idArr=idArr+','+resScr.insertId;
            });      
        },function(err){
            console.log(err);
            deff.reject(err);
        },function(succ){
            console.log('success');
            deff.resolve(idArr);
        });
        return deff.promise;
    }

    self.insertHeightDR=function(user_id,ftVal,inchVal,cmVal,recTime){
        var deff=$q.defer();
        var idArr='';
        DBInstance.transaction(function(tx){      
            tx.executeSql("insert into userHeight (server_userID, ftVal, inchVal, cmVal,recordDate, notes, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?)",[user_id,ftVal,inchVal,cmVal,recTime,"",new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                console.log('insertHeightDR inserted=>  ');
                idArr=idArr+','+resScr.insertId;
            });      
        },function(err){
            console.log(err);
            deff.reject(err);
        },function(succ){
            console.log('success');
            deff.resolve(idArr);
        });
        return deff.promise;
    }

    self.insertWaistDR=function(user_id,cmVal,inchVal,recTime){
        var deff=$q.defer();
        var idArr='';
        DBInstance.transaction(function(tx){      
            tx.executeSql("insert into userWaist (server_userID, cmVal, inchVal, recordDate, notes, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?)",[user_id,cmVal,inchVal,recTime,"",new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                console.log('insertWaistDR inserted=>  ');
                idArr=idArr+','+resScr.insertId;
            });      
        },function(err){
            console.log(err);
            deff.reject(err);
        },function(succ){
            console.log('success');
            deff.resolve(idArr);
        });
        return deff.promise;
    }

    self.fetchHeartRate = function(){
        var deff=$q.defer();     
        var heartArr = [];   
        var query="select * from heartRateDB where server_userID=?";
        DBInstance.transaction(function(tx){            
            tx.executeSql(query,[customService.carouselUserId],function(tx,res){
                for(var i=0;i<res.rows.length;i++){
                    heartArr.push(res.rows.item(i));
                }              
            });
        },function(err){
            console.log('fetchHeartRate_error');
            deff.reject(err);
        },function(succ){
            console.log('fetchHeartRate_success');
            deff.resolve(heartArr);
        });
        return deff.promise;
    }

    self.fetchRespiratoryRate = function(){
        var deff=$q.defer();     
        var resArr = [];   
        var query="select * from respRateDB where server_userID=?";
        DBInstance.transaction(function(tx){            
            tx.executeSql(query,[customService.carouselUserId],function(tx,res){
                for(var i=0;i<res.rows.length;i++){
                    resArr.push(res.rows.item(i));
                }              
            });
        },function(err){
            console.log('fetchRespiratoryRate_error');
            deff.reject(err);
        },function(succ){
            console.log('fetchRespiratoryRate_success');
            deff.resolve(resArr);
        });
        return deff.promise;
    }

    self.addPatient = function(data){
        var deff=$q.defer();
        $http({
           method: 'POST',
           url: baseUrl+'add_patient',
           data:data,
           "Content-Type": "application/json"
        })
        .then(function(objS){
            console.log("add_patient succ => "+objS);
            deff.resolve(objS);
        },function(objErr){
            console.log("add_patient err => "+objErr);
            deff.reject(objErr);
        })
        return deff.promise;
    }   

/******e-prescription insertion*******/
    self.uploadPres = function(obj){
        console.log('serv => '+JSON.stringify(obj));
        var deff=$q.defer();
        DBInstance.transaction(function(tx){
            tx.executeSql("insert into upload_Pres(dr_serverID, pat_serverID, health_condn, medication, provider, medical_his, remarks, test_recom_name, test_recom_notes, vi_temp, vi_HRate, vi_RRate, vi_bp, vi_bs, vi_weight, vi_height, vi_waist, init_date, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[obj.doctor_id,obj.patient_id,obj.condn,obj.medication,obj.provider,obj.medHistory,obj.remarksNotes,obj.test_rec_name,obj.test_rec_notes,obj.vi_temp,obj.vi_HRate,obj.vi_RRate,obj.vi_bp,obj.vi_bs,obj.vi_weight,obj.vi_height,obj.vi_waist,obj.uploadTime,new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                console.log('e-prescription inserted');
            });
        },function(err){
            console.log('upload pres error -> '+JSON.stringify(err));
            deff.reject(err);
        },function(succ){
            console.log('upload pres success');
            deff.resolve(succ);
        });
        return deff.promise;
    }

/******e-prescription fetch from db*******/
    self.fetchPres = function(patient_id){
        var deff=$q.defer();
        var recentData='';
        DBInstance.transaction(function(tx){
            tx.executeSql("select * from upload_Pres where pat_serverID=? ORDER BY init_date DESC",[patient_id],function(tx,res){
                //console.log('user fetched => '+JSON.stringify(res.rows));
                if(res.rows.length>0){
                   recentData= res.rows.item(0);
                }
           });
        },function(err){
            console.log('upload pres error -> '+JSON.stringify(err));
            deff.reject(err);
        },function(succ){
            console.log('upload pres success');
            deff.resolve(recentData);
        });
        return deff.promise;
    }

/******fetch patients from server and insert into db*******/
    self.insertPatientList = function(userID){
        var deff=$q.defer();
        $http({
           method: 'POST',
           url: baseUrl+'get_doctor_patient',
           data:{"user_id":userID},
           "Content-Type": "application/json"
        })
        .then(function(insSucc){
            console.log("insSucc=>>   "+insSucc);
            if(insSucc.data.responseCode==200){
                var arr = insSucc.data.responseData;
                DBInstance.transaction(function(tx){
                    var query = "DELETE FROM patientListDB";
                    tx.executeSql(query,[],function(txScr,resScr){
                        //succ
                    });
                    for(var i=0;i<arr.length;i++){
                        tx.executeSql("insert into patientListDB (dr_serverID, pat_serverID, family_patient_id, firstname, middlename, lastname, gender, dob, age, mobile, email, primaryAdd, street, city, state, country, pincode, patientType, familyPatientVerified, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[arr[i].dr_serverID,arr[i].pat_serverID,arr[i].family_patient_id,arr[i].firstname,arr[i].middlename,arr[i].lastname,arr[i].gender,arr[i].dob,arr[i].age,arr[i].mobile,arr[i].email,arr[i].primaryAdd,arr[i].street,arr[i].city,arr[i].state,arr[i].country,arr[i].pincode,arr[i].patientType,arr[i].familyPatientVerified,new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                            //console.log('sub profile inserted');
                        });
                    }
                },function(err){
                    console.log('ins error -> '+JSON.stringify(err));
                    deff.reject(err);
                },function(succ){
                    console.log('ins success-> '+JSON.stringify(succ));
                    deff.resolve(succ);
                });
            }
        },function(insErr){
            console.log("insErr=>>   "+JSON.stringify(insErr));
            deff.reject(insErr);
        })
        return deff.promise;
    }

/******fetch patient list from db*******/
    self.getPatientListFromDB = function(docID){
        var patListArr=[];
        var deff=$q.defer();
        DBInstance.transaction(function(tx){
            var query="select * from patientListDB where dr_serverID=? and delStatus=?";
            tx.executeSql(query,[parseInt(docID),'active'],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        patListArr.push(res.rows.item(i));
                    }
                }
            })
        },function(err){
            console.log('getPatientListFromDB error');
            deff.reject(err);
        },function(){
            console.log('getPatientListFromDB succ');
            deff.resolve(patListArr);
        })
        return deff.promise;
    }

/* Function for date range filter */
    self.ageRangeFinder = function(data,dateIDs){
        var status,year;
        switch(dateIDs){
            case 1: year=0; 
                    status=true;
                    break;
            case 2: year=12;
                    break;
            case 3: year=19;
                    break;
            case 4: year=34;
                    break;
            case 5: year=49;
                    break;
            case 6: year=64;
                    break;
            case 7: year=65;
                    break;
        }
        if(year!=0){
            if(year==12){
                if(data.age>=0 && data.age<=year)
                    status = true;
                else
                    status = false;
            }else if(year==19){
                if(data.age>=13 && data.age<=year)
                    status = true;
                else
                    status = false;
            }else if(year==34){
                if(data.age>=20 && data.age<=year)
                    status = true;
                else
                    status = false;
            }else if(year==49){
                if(data.age>=35 && data.age<=year)
                    status = true;
                else
                    status = false;
            }else if(year==64){
                if(data.age>=50 && data.age<=year)
                    status = true;
                else
                    status = false;
            }else if(year==65){
                if(data.age>=65)
                    status = true;
                else
                    status = false;
            }
        }        
        return status;
    }

    self.uploadDataInUpload_tbl = function(obj){
        var deff=$q.defer();
        var insID = "";
        DBInstance.transaction(function(tx){
            tx.executeSql("insert into upload_tbl (server_userID, category, provider, helthCond, uploadPic, uploadDoc, uploadDocDir, title, dateTime, notes, createdAt,  modifiedAt, providerId, conditionIds, initDate, uploaded_byID, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[obj.pat_serverID,obj.category,obj.provider,obj.helCond,obj.uploadPic,obj.uploadDoc,obj.uploadDocDir,obj.title,obj.time,obj.notes,obj.time,obj.time,obj.providerId,obj.helCondId,new Date().getTime(),obj.uploadedBy,new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                insID = resScr.insertId;
                console.log('upload data inserted');
            });
        },function(err){
            console.log('upload data error -> '+JSON.stringify(err));
            deff.reject(err);
        },function(succ){
            console.log('upload data success');
            deff.resolve(insID);
        }); 
        return deff.promise;
    }

    self.fetchUploadDataFromDB = function(drID){
        var deff=$q.defer();
        var uploadArr = [];
        //var query = "SELECT * FROM upload_tbl where uploaded_byID=? ORDER BY dateTime DESC";
        var query = "SELECT * FROM upload_tbl where delStatus='active' AND uploaded_byID=? ORDER BY dateTime DESC";
        DBInstance.transaction(function(tx){
            tx.executeSql(query,[drID],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        //console.log('res => '+JSON.stringify(res.rows.item(i)));
                        uploadArr.push(res.rows.item(i));
                    }
                }
            })
        },function(err){
            console.log('fetch upload data error -> '+JSON.stringify(err));
            deff.reject(err);
        },function(){
            console.log('fetch upload data success');
            deff.resolve(uploadArr);
        }); 
        return deff.promise;
    }

    self.findNoOfDays = function(years){
        var total_No_days=0;
        var currentYear = new Date().getFullYear();
        for(i=1;i<=years;i++){
            if(((currentYear % 4 == 0) && (currentYear % 100 != 0)) || (currentYear % 400 == 0)){
                total_No_days+=366;
            }
            else{
                total_No_days+=365;
            }
            currentYear--;
        }
        return total_No_days;            
    }

    /* Function for Date range filter */
    self.rangeFinderInitDate = function(data,dateIDs,customDate){
        //console.log("data->>  "+JSON.stringify(data));
        var status,days;
        switch(dateIDs){
            case 2: days=this.findNoOfDays(25);
                    break;
            case 3: days=this.findNoOfDays(10);
                    break;
            case 4: days=this.findNoOfDays(5);
                    break;
            case 5: days=this.findNoOfDays(1);
                    break;
            case 6: days=this.findNoOfDays(1)*0.5;
                    break;
            case 7: days=this.findNoOfDays(1)*0.25;
                    break;
            case 8: days=this.findNoOfDays(1)*(1/12);
                    break;
        }

        switch(dateIDs){
            case 1: status = true; 
                    break;
            case 2: if(new Date(data.initDate) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 3: if(new Date(data.initDate) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 4: if(new Date(data.initDate) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 5: if(new Date(data.initDate) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 6: if(new Date(data.initDate) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 7: if(new Date(data.initDate) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 8: if(new Date(data.initDate) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 9: if(new Date(data.initDate) > new Date().setTime(new Date().getTime() - 7*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 10:if(new Date(data.initDate) > new Date().setTime(customDate.fromDate) && new Date(data.initDate) < new Date().setTime(customDate.toDate) )
                        status = true;
                    else
                        status = false;
                    break;                                                                
        }
        return status;
    }

    /* Function for Upload date range filter */
    self.rangeFinderUploadDate = function(data,uploadDateIDs,upload_Date){
        //console.log("data->>  "+JSON.stringify(data));
        var status,days;
        switch(uploadDateIDs){
            case 2: days=this.findNoOfDays(25);
                    break;
            case 3: days=this.findNoOfDays(10);
                    break;
            case 4: days=this.findNoOfDays(5);
                    break;
            case 5: days=this.findNoOfDays(1);
                    break;
            case 6: days=this.findNoOfDays(1)*0.5;
                    break;
            case 7: days=this.findNoOfDays(1)*0.25;
                    break;
            case 8: days=this.findNoOfDays(1)*(1/12);
                    break;
        }

        switch(uploadDateIDs){
            case 1: status = true; 
                    break;
            case 2: if(new Date(data.dateTime) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 3: if(new Date(data.dateTime) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 4: if(new Date(data.dateTime) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 5: if(new Date(data.dateTime) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 6: if(new Date(data.dateTime) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 7: if(new Date(data.dateTime) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 8: if(new Date(data.dateTime) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 9: if(new Date(data.dateTime) > new Date().setTime(new Date().getTime() - 7*24*60*60*1000))
                        status = true;
                    else
                        status = false;
                    break;
            case 10:if(new Date(data.dateTime) > new Date().setTime(upload_Date.fromDate) && new Date(data.dateTime) < new Date().setTime(upload_Date.toDate) )
                        status = true;
                    else
                        status = false;
                    break;                                                                
        }
        return status;
    }

    /* Function for fetching the latest record from weight table for patient */
    self.fetchWeightForPersonal = function(userID){
        var deff=$q.defer();
        var latestData = {};
        var query="select * from userWeight where status=? AND server_userID=? ORDER BY recordDate DESC LIMIT 1";
        DBInstance.transaction(function(tx){
            tx.executeSql(query,['active',userID],function(tx,res){
                if(res.rows.length>0) {
                    latestData = res.rows.item(0)
                }
            })
        },function(err){
            console.log('fetch weight error -> '+err);
            deff.reject(err);
        },function(){
            console.log('fetch weight success');
            deff.resolve(latestData);
        }); 
        return deff.promise;
    }

    /* Function for fetching the latest record from height table for patient */
    self.fetchHeightForPersonal = function(userID){
        var deff=$q.defer();
        var latestData = {};
        var query="select * from userHeight where status=? AND server_userID=? ORDER BY recordDate DESC LIMIT 1";
        DBInstance.transaction(function(tx){
            tx.executeSql(query,['active',userID],function(tx,res){
                if(res.rows.length>0) {
                    latestData = res.rows.item(0)
                }
            })
        },function(err){
            console.log('fetch weight error -> '+err);
            deff.reject(err);
        },function(){
            console.log('fetch weight success');
            deff.resolve(latestData);
        }); 
        return deff.promise;
    }

    /* Function for fetching the latest record from waist table for patient */
    self.fetchWaistForPersonal = function(userID){
        var deff=$q.defer();
        var latestData = {};
        var query="select * from userWaist where status=? AND server_userID=? ORDER BY recordDate DESC LIMIT 1";
        DBInstance.transaction(function(tx){
            tx.executeSql(query,['active',userID],function(tx,res){
                if(res.rows.length>0) {
                    latestData = res.rows.item(0)
                }
            })
        },function(err){
            console.log('fetch weight error -> '+err);
            deff.reject(err);
        },function(){
            console.log('fetch weight success');
            deff.resolve(latestData);
        }); 
        return deff.promise;
    }

    /*************** Fetch data from VS_bloodPressure DB for Patient ************/
    self.fetchBPForPatient = function(userID){
        var deff=$q.defer();     
        var bloodPreArr = [];   
        var query="select * from VS_bloodPressure where status=? AND server_userID=? ORDER BY recordDateTime DESC";
        DBInstance.transaction(function(tx){            
            tx.executeSql(query,['active',userID],function(tx,res){                
                for(var i=0;i<res.rows.length;i++){
                    bloodPreArr.push({"bpId":res.rows.item(i).uniId,"uniId":res.rows.item(i).uniId,"type":"bp","server_userID":res.rows.item(i).server_userID,"systolicVal":res.rows.item(i).systolicVal,"diastolicVal":res.rows.item(i).diastolicVal,"recordDateTime":res.rows.item(i).recordDateTime,"notes":res.rows.item(i).notes,"status":res.rows.item(i).status,"modifiedAt":res.rows.item(i).modifiedAt,"category":"Blood Pressure"});
                }                
            });
        },function(err){
            console.log('fetchBloodPressure_error');
            deff.reject(err);
        },function(succ){
            console.log('fetchBloodPressure_success');
            deff.resolve(bloodPreArr);
        });
        return deff.promise;
    }

    /*************** Fetch data from VS_bloodPressure DB for Patient ************/
    self.fetchBSForPatient = function(userID){
        var deff=$q.defer();     
        var bloodSugarArr = [];   
        var query="select * from VS_bloodSugar where status=? AND server_userID=? ORDER BY recordDateTime DESC";
        DBInstance.transaction(function(tx){            
            tx.executeSql(query,['active',userID],function(tx,res){    
                for(var i=0;i<res.rows.length;i++){
                    bloodSugarArr.push({"uniId":res.rows.item(i).uniId,"sugarId":res.rows.item(i).uniId,"type":"sugar","server_userID":res.rows.item(i).server_userID,"sugarVal":res.rows.item(i).sugarVal,"mealType":res.rows.item(i).mealType,"recordDateTime":res.rows.item(i).recordDateTime,"notes":res.rows.item(i).notes,"status":res.rows.item(i).status,"modifiedAt":res.rows.item(i).modifiedAt,"category":"Blood Sugar"});
                }           
            });
        },function(err){
            console.log('fetchBloodSugar_error');
            deff.reject(err);
        },function(succ){
            console.log('fetchBloodSugar_success');
            deff.resolve(bloodSugarArr);
        });
        return deff.promise;
    }

    /*************** Fetch data from VS_temperature DB for patient ************/
    self.fetchTempForPatient = function(userID){
        var deff=$q.defer();     
        var tempArr = [];   
        var query="select * from VS_temperature where status=? AND server_userID=? ORDER BY recordDateTime DESC";
        DBInstance.transaction(function(tx){            
            tx.executeSql(query,['active',userID],function(tx,res){
                for(var i=0;i<res.rows.length;i++){
                    tempArr.push({"uniId":res.rows.item(i).uniId,"temperatureId":res.rows.item(i).uniId,"type":"temperature","server_userID":res.rows.item(i).server_userID,"fValue":res.rows.item(i).fValue,"cValue":res.rows.item(i).cValue,"recordDateTime":res.rows.item(i).recordDateTime,"notes":res.rows.item(i).notes,"status":res.rows.item(i).status,"modifiedAt":res.rows.item(i).modifiedAt,"category":"Temperature"});
                }              
            });
        },function(err){
            console.log('fetchTemperature_error');
            deff.reject(err);
        },function(succ){
            console.log('fetchTemperature_success');
            deff.resolve(tempArr);
        });
        return deff.promise;
    }

    /*************** Fetch data from heartRateDB DB for patient ************/
    self.fetchHeartRateForPatient = function(userID){
        var deff=$q.defer();     
        var heartArr = [];   
        var query="select * from heartRateDB where server_userID=? ORDER BY recTime DESC";
        DBInstance.transaction(function(tx){            
            tx.executeSql(query,[userID],function(tx,res){
                for(var i=0;i<res.rows.length;i++){
                    heartArr.push({"uniId":res.rows.item(i).uniId,"server_userID":res.rows.item(i).server_userID,"recTime":res.rows.item(i).recTime,"value":res.rows.item(i).value,"initDate":res.rows.item(i).initDate,"modifiedAt":res.rows.item(i).recTime,"category":"Heart Rate"});
                }              
            });
        },function(err){
            console.log('fetch heart error');
            deff.reject(err);
        },function(succ){
            console.log('fetch heart success');
            deff.resolve(heartArr);
        });
        return deff.promise;
    }

    /*************** Fetch data from respRateDB DB for patient ************/
    self.fetchResRateForPatient = function(userID){
        var deff=$q.defer();     
        var resRateArr = [];   
        var query="select * from respRateDB where server_userID=? ORDER BY recTime DESC";
        DBInstance.transaction(function(tx){            
            tx.executeSql(query,[userID],function(tx,res){
                for(var i=0;i<res.rows.length;i++){
                    resRateArr.push({"uniId":res.rows.item(i).uniId,"server_userID":res.rows.item(i).server_userID,"recTime":res.rows.item(i).recTime,"value":res.rows.item(i).value,"initDate":res.rows.item(i).initDate,"modifiedAt":res.rows.item(i).recTime,"category":"Respiratory Rate"});
                }              
            });
        },function(err){
            console.log('fetch res error');
            deff.reject(err);
        },function(succ){
            console.log('fetch res success');
            deff.resolve(resRateArr);
        });
        return deff.promise;
    }

    /* Function for Patient VS */
    self.rangeFinder = function(data,dateIDs){
        var status,days;
        switch(dateIDs){
            case 1: days=0; 
                    status=true;
                    break;
            case 2: days=30;
                    break;
            case 3: days=90;
                    break;
            case 4: days=this.findNoOfDays(1);
                    break;
            case 5: days=this.findNoOfDays(2);
                    break;
            case 6: days=this.findNoOfDays(3);
                    break;
        }
        if(days!=0){
            if(new Date(data.modifiedAt) > new Date().setTime(new Date().getTime() - days*24*60*60*1000))
                status = true;
            else
                status = false;
        }        
        return status;
    }

    /* Fetch Provider list from db for Patient RSP Screen */
    self.getProviderOfPatient = function(userID){
        var deff=$q.defer();
        var providerList = [];
        DBInstance.transaction(function(tx){
            var query="select * from userProviders where server_ProviderID=?";
            tx.executeSql(query,[userID],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        providerList.push(res.rows.item(i));
                    }
                }
            })
        },function(err){
            console.log('fetch providers error');
            deff.reject(err);
        },function(){
            console.log('fetch providers succ');
            deff.resolve(providerList);
        });
        return deff.promise;
    }

    /* Fetch active health condition list from db for Patient RSP Screen */
    self.getHelCondOfPatient = function(userID){
        var deff=$q.defer();
        var arrForHlthCondHint=[];
        DBInstance.transaction(function(tx){
            var query = "select * from userPreexistCondn where delStatus='active' AND server_userID=?";
            tx.executeSql(query,[userID],function(tx,res1){
                for(var i=0;i<res1.rows.length;i++){
                    arrForHlthCondHint.push(res1.rows.item(i).preexistName);
                }                              
                //console.log('arrForHlthCondHint -> '+JSON.stringify(arrForHlthCondHint));
            });
        },function(err){
            console.log('fetch hel cond error');
            deff.reject(err);
        },function(){
            console.log('fetch hel cond succ');
            deff.resolve(arrForHlthCondHint);
        });
        return deff.promise;
    }

    /* Fetch data from upload_tbl db for Patient RSP Screen */
    self.getUploadList = function(userID){
        var deff=$q.defer();
        var uploadDataList=[];
        DBInstance.transaction(function(tx){
            var query = "SELECT * FROM upload_tbl where server_userID=? AND delStatus='active' ORDER BY dateTime DESC";
            //console.log("query=>  "+query);
            tx.executeSql(query,[userID],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        //console.log('res => '+JSON.stringify(res.rows.item(i)));
                        uploadDataList.push({"type":res.rows.item(i).category,"uploadId":res.rows.item(i).uniId,"uniId":res.rows.item(i).uniId,"server_userID":res.rows.item(i).server_userID,"category":res.rows.item(i).category,"providerId":res.rows.item(i).providerId,"provider":res.rows.item(i).provider,"helthCond":res.rows.item(i).helthCond,"uploadPic":res.rows.item(i).uploadPic,"uploadDoc":res.rows.item(i).uploadDoc,"uploadDocDir":res.rows.item(i).uploadDocDir,"title":res.rows.item(i).title,"dateTime":res.rows.item(i).dateTime,"notes":res.rows.item(i).notes,"createdAt":res.rows.item(i).createdAt,"modifiedAt":res.rows.item(i).modifiedAt,"providerId":res.rows.item(i).providerId,"conditionIds":res.rows.item(i).conditionIds,"uploaded_byID":res.rows.item(i).uploaded_byID});
                    }
                }                              
            });
        },function(err){
            console.log('fetch upload list error');
            deff.reject(err);
        },function(){
            console.log('fetch upload list succ');
            deff.resolve(uploadDataList);
        });
        return deff.promise;
    }

    /* Function for fetching the medicine list for patient from DB */
    self.getMedicineList = function(userID){
        var deff=$q.defer();
        var medList=[];
        DBInstance.transaction(function(tx){
            var query="SELECT * FROM (SELECT * FROM addMedication where status='active' AND delStatus='active' AND server_userID=? ORDER BY start_date) UNION ALL SELECT * FROM(SELECT * FROM addMedication where status='inactive' AND delStatus='active' AND server_userID=? ORDER BY curdDate)";
            //console.log("query=>  "+query);
            tx.executeSql(query,[userID,userID],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        medList.push(res.rows.item(i));
                    }
                }                              
            });
        },function(err){
            console.log('fetch med list error');
            deff.reject(err);
        },function(){
            console.log('fetch med list succ');
            deff.resolve(medList);
        });
        return deff.promise;
    }

    /* Fetch data from upload_tbl DB to view on upload view screen */
    self.getDataForView = function(uniID){
        var deff=$q.defer();
        var data = [];
        DBInstance.transaction(function(tx){
            var query="select * from upload_tbl where uniId=?";
            tx.executeSql(query,[uniID],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        data.push(res.rows.item(i));
                    }
                }
            })
        },function(err){
            console.log('getDataForView error');
            deff.reject(err);
        },function(){
            console.log('getDataForView succ');
            deff.resolve(data);
        });
        return deff.promise;
    }
    
    /* Fetch all the prescription for logged in doctor */
    self.getAllPresOfDr = function(){
        var deff=$q.defer();
        var data = [];
        DBInstance.transaction(function(tx){
            var query="select * from upload_Pres where dr_serverID=? AND delStatus='active' ORDER BY init_date DESC";
            tx.executeSql(query,[parseInt(localStorage.getItem('loginUserId'))],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        data.push(res.rows.item(i));
                    }
                }
            })
        },function(err){
            console.log('getAllPresOfDr error');
            deff.reject(err);
        },function(){
            console.log('getAllPresOfDr succ');
            deff.resolve(data);
        });
        return deff.promise;
    }

    /* Insert logged in doctor as a provider in userProviders tbl */
    self.instDrAsProvider = function(obj){
        var deff=$q.defer();
        var insID='';
        DBInstance.transaction(function(tx){
            tx.executeSql("insert into userProviders (server_ProviderID, firstname, middlename, lastname, role, img, gender, age, mobile, email, addressType, primaryAdd, street, city, state, pincode, specialization, experience, status, createdAt, modifiedAt, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[obj.id,obj.fName,obj.mName,obj.lName,"Doctor","","","",obj.mobile,obj.email,"","","","","","","","","Not Verify",new Date().getTime(),new Date().getTime(),new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                console.log('inserted');
                insID=resScr.insertId;
           });
        },function(err){
            console.log('instDrAsProvider err');
            deff.reject(err);
        },function(succ){
            console.log('instDrAsProvider succ');
            deff.resolve(insID);
        });   
        return deff.promise;
    }

    /******* Fetch doctor provider List from database **********/

    self.fetchDrProviderList=function(providerRole){
        var providerList=[];
        var deff=$q.defer();
        DBInstance.transaction(function(tx){
            for(i=0;i<=providerRole.length;i++) {
                /* Updated by Anoop - for logged in user providers */
                var query="select a.providerId,a.server_ProviderID,a.firstname,a.middlename,a.lastname,a.role,a.img,a.gender,a.age,a.mobile,a.email,a.addressType,a.primaryAdd,a.street,a.city,a.state,a.pincode,a.specialization,a.experience,a.status,a.createdAt,a.modifiedAt,b.uniId,b.medical_name,b.delStatus,c.uniId as uploadId,c.title,c.category from userProviders a left join addMedication b on a.providerId = b.consultantId and b.delStatus=? and b.server_userID=? left join upload_tbl c on a.providerId = c.providerId and c.server_userID=? and c.delStatus=? where role=? AND a.server_ProviderID=? AND a.providerId!=?";

                tx.executeSql(query,['active',customService.carouselUserId,customService.carouselUserId,'active',providerRole[i],parseInt(localStorage.getItem('loginUserId')),parseInt(localStorage.getItem('proIDForDr'))],function(tx,res){
                    if(res.rows.length>0){
                        for(var i=0;i<res.rows.length;i++){
                            //console.log('items => '+JSON.stringify(res.rows.item(i)));
                            var index = providerList.findIndex(function(list){
                                return list.providerId == res.rows.item(i).providerId
                            })
                            //console.log("provider_id: "+index);
                            if(index==-1){
                                providerList.push({"type":"provider","providerId":res.rows.item(i).providerId,"serverProviderId":res.rows.item(i).server_ProviderID,"firstName":res.rows.item(i).firstname,"middlename":res.rows.item(i).middlename,"lastname":res.rows.item(i).lastname,"role":res.rows.item(i).role,"img":res.rows.item(i).img,"gender":res.rows.item(i).gender,"age":res.rows.item(i).age,"mobile":res.rows.item(i).mobile,"email":res.rows.item(i).email,"addressType":res.rows.item(i).addressType,"primaryAdd":res.rows.item(i).primaryAdd,"street":res.rows.item(i).street,"city":res.rows.item(i).city,"state":res.rows.item(i).state,"pincode":res.rows.item(i).pincode,"specialization":res.rows.item(i).specialization,"experience":res.rows.item(i).experience,"status":res.rows.item(i).status,"createdAt":res.rows.item(i).createdAt,"modifiedAt":res.rows.item(i).modifiedAt});
                                
                                providerList[providerList.length-1].medicineArr=[];
                                providerList[providerList.length-1].medicineArr.push({medicine:res.rows.item(i).medical_name,
                                    medicineId:res.rows.item(i).uniId
                                });
                                providerList[providerList.length-1].uploadArr=[];
                                providerList[providerList.length-1].uploadArr.push({
                                    title:res.rows.item(i).title,
                                    uploadId:res.rows.item(i).uploadId,
                                    category:res.rows.item(i).category
                                });
                            }
                            else{
                                providerList[index].medicineArr.push({medicine:res.rows.item(i).medical_name,
                                    medicineId:res.rows.item(i).uniId
                                });
                                providerList[providerList.length-1].uploadArr.push({
                                    title:res.rows.item(i).title,
                                    uploadId:res.rows.item(i).uploadId,
                                    category:res.rows.item(i).category
                                });
                            }            
                        }
                    }
                })
            }
        },function(err){
            console.log('ProviderList db error: '+JSON.stringify(err));
            deff.reject(err);
        },function(){
            console.log('ProviderList db succ: ');
            deff.resolve(providerList);
        })
        return deff.promise;
    }

    /* Update doctor data in user tbl */
    self.updateDrInfoInUserTbl = function(fullN,email,id){
        var deff=$q.defer();
        if(fullN.indexOf(" ")==-1) {
            var firstName = fullN;
            var middleName = '';
            var lastName = '';
        } else if(fullN.split(" ").length==2) {
            var firstName = fullN.split(' ')[0];
            var middleName = '';
            var lastName = fullN.split(' ')[1];
        } else if(fullN.split(" ").length>=3) {
            var firstName = fullN.split(' ')[0];
            var middleName = fullN.split(' ')[1];
            var lastName = fullN.split(' ')[2];
        }
        var columnNArr = ['email','firstname','middlename','lastname','updtDate','sync'];
        var ArrData = [email,firstName,middleName,lastName,new Date().getTime(),"0",id];
        DBInstance.transaction(function(tx){
            var setMltplData = '';
            for(var i=0;i<columnNArr.length;i++) {
                if(i==columnNArr.length-1) {
                   setMltplData = setMltplData + columnNArr[i]+'=?'
                } else {
                   setMltplData = setMltplData + columnNArr[i]+'=?, '
                }
            }
           var query="UPDATE users SET "+setMltplData+" where server_userID=?";
            //var query="UPDATE users SET email="+email+" AND firstname="+firstName+" AND middlename="+middleName+" AND lastname="+lastName+" where server_userID="+id;
            console.log('query:-   '+query);
            tx.executeSql(query,ArrData,function(tx,res){
               console.log('dr user update succ');
            },function(err){
               console.log('dr user update err'+err)
            })
        },function(err){
            console.log('update db error');
            deff.reject(err);
        },function(succ){
            console.log('update db succ');
            deff.resolve(succ);
        })
        return deff.promise;
    }

    /* Functions for getting data for recall patient last reading functionality */
    self.getLatestDataForRecall = function(tbl,clmn,delStatus){
        var deff=$q.defer();
        var data = [];
        DBInstance.transaction(function(tx){
            var query="select * from "+tbl+" where server_userID=? AND "+delStatus+"=? ORDER BY "+clmn+" DESC LIMIT 1";
            tx.executeSql(query,[customService.carouselUserId,'active'],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        data.push(res.rows.item(i));
                    }
                }
            });
        },function(err){
            console.log('get latest error');
            deff.reject(err);
        },function(){
            console.log('get latest succ');
            deff.resolve(data);
        });
        return deff.promise;
    }

    /* Functions for inserting test list into DB for e-prescription */
    self.insertTestList = function(){
        var deff=$q.defer();
        $http({
           method: 'GET',
           url: baseUrl+'getTestList', 
           "Content-Type": "application/json"
        })
        .then(function(objS){
            //console.log('insertOccupation_succ -> '+JSON.stringify(objS));
            if(objS.data.responseCode==200){
                var testArr = objS.data.responseData;
                DBInstance.transaction(function(tx){
                    tx.executeSql("DELETE FROM testList",[],function(txScr,resScr){
                        //succ
                    });
                    for(var i=0;i<testArr.length;i++){
                        tx.executeSql("insert into testList (testID, testName, initDate, updtDate, local_id) values (?,?,?,?,?)",[testArr[i].id,testArr[i].name,new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                            //console.log('occupations inserted');
                        });
                    }
                },function(err){
                    console.log('occupations error -> '+JSON.stringify(err));
                    deff.reject(err);
                },function(succ){
                    console.log('occupations success');
                    deff.resolve(succ);
                });
            }
        },function(objE){
           console.log("insertOccupation_err ->-->> "+JSON.stringify(objE));
           deff.reject(objE);
        });
        return deff.promise;
    }

    /* Fetch test list from DB*/
    self.fetchTestList = function(){
        var testListArr=[];
        var deff=$q.defer();
        DBInstance.transaction(function(tx){
            var query="select * from testList";
            tx.executeSql(query,[],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        testListArr.push({"id":res.rows.item(i).testID,"name":res.rows.item(i).testName});
                    }
                }
            })
        },function(err){
            console.log('fetchOccupation error');
            deff.reject(err);
        },function(){
            console.log('fetchOccupation succ');
            deff.resolve(testListArr);
        })
        return deff.promise;
    }

    /* Functions to check dr is approved by admin or Not */
    self.checkDrApprovedByAdmin = function(userID){
        var deff=$q.defer();
        $http({
           method: 'POST',
           url: baseUrl+'check_doctor_status',
           data:{"doctor_id":userID},
           "Content-Type": "application/json"
        })
        .then(function(objS){
            console.log("checkDrApprovedByAdmin_succ");
            deff.resolve(objS);
        },function(objE){
            console.log("checkDrApprovedByAdmin_err");
            deff.reject(objE);
        });
        return deff.promise;
    }

    /* Functions for delete data from DoctorDetail tbl */
    self.updateDoctorDet = function(obj){
        var deff=$q.defer();
        DBInstance.transaction(function(tx){
            //var query = "DELETE FROM DoctorDetail";
            var query="UPDATE DoctorDetail SET verify_status=?, varified_time=?, updtDate=?, sync=? where server_userID=?";
            tx.executeSql(query,[obj.verify_status,obj.varified_time,new Date().getTime(),"0",obj.id],function(tx,res){
                //succ
            });
        },function(err){
            console.log('updateDoctorDet error');
            deff.reject(err);
        },function(succ){
            console.log('updateDoctorDet succ');
            deff.resolve(succ);
        });
        return deff.promise;
    }
})

/* Filter for patient list Screen*/
.filter('filterForPatient',function(doctor){
    return function(input,ageIDs,patientIDs,genderIDs){
        if(ageIDs==0 && patientIDs==0 && genderIDs==0){
            return input;
        }else if(ageIDs!=0 && patientIDs==0 && genderIDs==0){
            return input.filter(function(year){
                return doctor.ageRangeFinder(year,ageIDs);
            });
        }else{
            return input.filter(function(year){
                return ((patientIDs!=0?patientIDs==year.patientType:true) && (genderIDs!=0?genderIDs==year.gender:true)) && (ageIDs!=0?doctor.ageRangeFinder(year,ageIDs):true)
            });
        }  
    }
})
.filter('filterForRest',function(doctor){
    return function(input,dateIDs,uploadDateIDs,categoryIDs,patientIDsUP,verificationIDs,uploadedByIDs,customDate,upload_Date){
        if(dateIDs==0 && uploadDateIDs==0 && categoryIDs.length==0 && patientIDsUP.length==0 && verificationIDs==0 && uploadedByIDs.length==0){
            return input;
        }else if(dateIDs!=0 && uploadDateIDs==0 && categoryIDs.length==0 && patientIDsUP.length==0 && verificationIDs==0 && uploadedByIDs.length==0){
            return input.filter(function(year){
                return doctor.rangeFinderInitDate(year,dateIDs,customDate);
            });
        }else if(dateIDs==0 && uploadDateIDs!=0 && categoryIDs.length==0 && patientIDsUP.length==0 && verificationIDs==0 && uploadedByIDs.length==0){
            return input.filter(function(year){
                return doctor.rangeFinderUploadDate(year,uploadDateIDs,upload_Date);
            });
        }else{
            return input.filter(function(year){
                return ((categoryIDs.length!=0?categoryIDs.indexOf(year.category)!=-1:true) && (patientIDsUP.length!=0?patientIDsUP.indexOf(year.server_userID)!=-1:true) && (verificationIDs!=0?verificationIDs==year.verification:true) && (uploadedByIDs.length!=0?uploadedByIDs.indexOf(year.uploaded_byID)!=-1:true)) && (dateIDs!=0 ? doctor.rangeFinderInitDate(year,dateIDs,customDate) : true) && (uploadDateIDs!=0 ? doctor.rangeFinderUploadDate(year,uploadDateIDs,upload_Date) : true)
            });
        }  
    }
})

/* Filter for Patient VS Screen*/
.filter('filterForVSPat',function(doctor){
    return function(input,dateIDs,vitalSymIDs){
        if(dateIDs==0 && vitalSymIDs.length==0){
            return input;
        }else if(dateIDs!=0 && vitalSymIDs.length==0){
            return input.filter(function(year){
                return doctor.rangeFinder(year,dateIDs);
            });
        }else{
            return input.filter(function(data){
                return ((vitalSymIDs.length!=0 && vitalSymIDs.indexOf(data.category)!=-1)) && (dateIDs!=0 ? doctor.rangeFinder(data,dateIDs) : true)
            });
        }  
    }
})



