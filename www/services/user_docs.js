angular.module('PatientRecord.docs', [])
.service('docs',['$q','$http','DBInstance','baseUrl','user','$cordovaFile','$timeout','customService','$rootScope',function($q, $http, db, baseUrl, user, $cordovaFile, $timeout, customService,$rootScope) {
    var self = this;

    /***********inserting documents into local db************/       
    self.insertDoc=function(docInfo){
        var deff=$q.defer();
        db.transaction(function(tx){
            tx.executeSql("INSERT INTO add_documentation (server_userID, doc_type, title, valid_from, valid_until, notes, doc, docDir, picture, createdAt, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?)",[docInfo.user_id, docInfo.document, docInfo.title, docInfo.from, docInfo.to, docInfo.notes, docInfo.uploadDoc, docInfo.uploadDocDir, docInfo.images, new Date().getTime(), new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                        //succ
            }); 
        },function(err){
            deff.reject(err);
        },function(){
            deff.resolve("documents added");
        });
        return deff.promise;
    }
     
    /***********fetch documents from local db************/       
    self.fetchDoc=function(){
        var deff=$q.defer();
        var documentsList=[];
        db.transaction(function(tx){
            var query="select * from add_documentation where status='active' AND server_userID=?";
            tx.executeSql(query,[customService.carouselUserId],function(tx,res){
                for(var i=0;i<res.rows.length;i++){
                    //documentsList.push(res.rows.item(i));
                    documentsList.push({uniId:res.rows.item(i).uniId, type:"documentation", documentationId:res.rows.item(i).uniId,server_userID:res.rows.item(i).server_userID, doc_type:res.rows.item(i).doc_type, title:res.rows.item(i).title, valid_from:res.rows.item(i).valid_from, valid_until:res.rows.item(i).valid_until, notes:res.rows.item(i).notes, doc:res.rows.item(i).doc, docDir:res.rows.item(i).docDir, picture:res.rows.item(i).picture,modifiedAt:res.rows.item(i).createdAt,initDate:res.rows.item(i).initDate});
                }
            }) 
        },function(err){
            deff.reject(err);
        },function(succ){
            deff.resolve(documentsList)
        });
        return deff.promise;
    }

    /***********Copt pdf files into application folder************/
    self.copyPdf=function(docArr,count,_deff){
        var deff=_deff?_deff:$q.defer();
        //console.log("cordovaFile->>   "+JSON.stringify($cordovaFile));
        $cordovaFile.copyFile(docArr[count].fileDirectory,docArr[count].oldFileName,cordova.file.externalApplicationStorageDirectory)
        .then(function (success) {
            console.log("Document successfully copied => "+JSON.stringify(success));
            if(count != docArr.length-1){
                ++count;
                console.log("count service => "+count);
                self.copyPdf(docArr,count,deff);
            }else{
                deff.resolve('success');
            }
        }, function (error) {
            console.log("doc service Error: " + JSON.stringify(error));
            deff.reject(error);
        });
        return deff.promise;
    }

    /********* Functions for fetching data from db for share screen *********/

    self.fetchUserPreexistCondnForShare=function(){
        var deff=$q.defer();
        var userPreCondnList=[];
        db.transaction(function(tx){
            //var query = "SELECT * FROM userPreexistCondn where delStatus=?";
            //var query="select a.uniId,a.server_userID,a.preexistName,a.findingDate,a.curedDate,a.status,a.delStatus,a.createdAt,a.modifiedAt,a.initDate,b.uniId as medUniId,b.server_userID as medServerId,b.medical_name,b.conditionId,b.delStatus as deleteSta,c.providerId,c.server_ProviderID,c.firstname from userPreexistCondn a left join addMedication b on a.uniId = b.conditionId left join userProviders c on c.providerId = b.consultantId where a.delStatus=? and deleteSta=? and a.server_userID=? and medServerId=? and c.server_ProviderID=?";
            
            var query="select a.uniId, a.server_userID, a.preexistName, a.findingDate, a.curedDate, a.status, a.delStatus, a.createdAt, a.modifiedAt, a.initDate, b.uniId as medUniId, b.server_userID as medServerId, b.medical_name, b.conditionId, b.delStatus as deleteSta, c.providerId, c.server_ProviderID, c.firstname from userPreexistCondn a left join addMedication b on a.uniId = b.conditionId and b.delStatus=? and b.server_userID=? left join userProviders c on c.providerId = b.consultantId and c.server_ProviderID=? where a.delStatus=? and a.server_userID=?";

            tx.executeSql(query,['active',customService.carouselUserId,parseInt(localStorage.getItem('loginUserId')),'active',customService.carouselUserId],function(tx,res){ 
                if(res.rows.length>0){
                    //console.log('JSON rows => '+JSON.stringify(res.rows));
                    for(var i=0;i<res.rows.length;i++){
                        //var index=userPreCondnList.findIndex(x => x.uniId==res.rows.item(i).uniId);
                        var index = userPreCondnList.findIndex(function(list){
                                return list.uniId == res.rows.item(i).uniId
                            })
                        if(index==-1){
                            userPreCondnList.push(res.rows.item(i));
                            userPreCondnList[userPreCondnList.length-1].medicineArr=[];
                            userPreCondnList[userPreCondnList.length-1].providerArr=[];
                            if(res.rows.item(i).conditionId!=null){
                                userPreCondnList[userPreCondnList.length-1].medicineArr.push(
                                    {medicine:res.rows.item(i).medical_name,
                                     medicineId:res.rows.item(i).medUniId
                                    });
                            }
                            if(res.rows.item(i).providerId!=null){
                                userPreCondnList[userPreCondnList.length-1].providerArr.push(
                                    {provName:res.rows.item(i).firstname,
                                     provId:res.rows.item(i).providerId
                                    });
                            }
                        }else{
                            if(res.rows.item(i).conditionId!=null){
                                userPreCondnList[index].medicineArr.push({
                                    medicine:res.rows.item(i).medical_name,
                                    medicineId:res.rows.item(i).medUniId
                                });
                            }
                            if(res.rows.item(i).providerId!=null){
                                userPreCondnList[index].providerArr.push({
                                    provName:res.rows.item(i).firstname,
                                    provId:res.rows.item(i).providerId
                                });
                            }
                        }       
                    }
                }               
            })
        },function(err){
            console.log('user pre exist db error');
            deff.reject(err);
        },function(){
            deff.resolve(userPreCondnList);
            console.log('user pre exist db succ');
        })
        return deff.promise;
    }

    self.fetchMedicineForShare=function(){
        var deff=$q.defer();
        db.transaction(function(tx){
            var query = "SELECT * FROM addMedication where delStatus='active' AND server_userID=?";
            tx.executeSql(query,[customService.carouselUserId],function(tx,res){
                var userMedicineList=[];
                for(var i=0;i<res.rows.length;i++){
                    userMedicineList.push(res.rows.item(i));
                }
                deff.resolve(userMedicineList);
            })
        },function(err){
            console.log('user pre exist db error');
            deff.reject(err);
        },function(){
            console.log('user pre exist db succ');
        })
        return deff.promise;
    }

    self.fetchUserAllergyForShare=function(){
        var deff=$q.defer();
        db.transaction(function(tx){
            var query = "SELECT * FROM userAllergies where delStatus='active' AND server_userID=?";
            tx.executeSql(query,[customService.carouselUserId],function(tx,res){
                var userAllergyList=[];
                for(var i=0;i<res.rows.length;i++){
                    userAllergyList.push(res.rows.item(i));
                }
                deff.resolve(userAllergyList);
            })
        },function(err){
            console.log('user allergy db error');
            deff.reject(err);
        },function(){
            console.log('user allergy db succ');
        })
        return deff.promise;
    }

    self.fetchUploadDataForShare = function(){
        var uploadDataList=[];
        var deff=$q.defer();
        db.transaction(function(tx){
            var query = "SELECT * FROM upload_tbl where server_userID=? AND delStatus='active'";
            tx.executeSql(query,[customService.carouselUserId],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        uploadDataList.push(res.rows.item(i));
                    }
                }
            })
        },function(err){
            console.log('fetchUploadDataForShare error');
            deff.reject(err);
        },function(){
            console.log('fetchUploadDataForShare succ');
            deff.resolve(uploadDataList);
        });
        return deff.promise;
    }

    self.getUserVaccinesForShare = function(){
        var deff=$q.defer();
        var query="SELECT * FROM userVaccines where status='active' AND server_userID=?";
        db.transaction(function(tx){
            tx.executeSql(query,[customService.carouselUserId],function(tx,res){
                var userVaccineData = [];
                for(var i=0;i<res.rows.length;i++){
                    var index=userVaccineData.findIndex(x => x.vaccineID==res.rows.item(i).vaccineID);
                    if(index==-1)
                        userVaccineData.push(res.rows.item(i));
                }
                //console.log('userVaccineData -> '+JSON.stringify(userVaccineData));
                deff.resolve(userVaccineData);
            });
        },function(err){
            console.log('get user vaccines error');
            deff.reject(err);
        },function(succ){
            console.log('get user vaccines success');
        });
        return deff.promise;
    }

    self.fetchDocForShare = function(){
        var deff=$q.defer();
        var documentsList=[];
        db.transaction(function(tx){
            var query="select * from add_documentation where status='active' AND server_userID=?";
            tx.executeSql(query,[customService.carouselUserId],function(tx,res){
                for(var i=0;i<res.rows.length;i++){
                    //documentsList.push(res.rows.item(i));
                    documentsList.push(res.rows.item(i));
                }
            }) 
        },function(err){
            deff.reject(err);
        },function(succ){
            deff.resolve(documentsList)
        });
        return deff.promise;
    }

    self.fetchEmergencyContactForShare = function(){
        var contact = [];
        var deff=$q.defer();
        db.transaction(function(tx){
            var query="select * from emergencyContact where server_userID=?";
            tx.executeSql(query,[customService.carouselUserId],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        contact.push(res.rows.item(i));
                    }   
                }
            })
        },function(err){
            console.log('User emergency db error');
            deff.reject(err);
        },function(){
            console.log('User emergency db succ');
            deff.resolve(contact);
        })
        return deff.promise;
    }
    
    self.fetchProviderForShare=function(providerRole){
        var providerArr=[];
        var deff=$q.defer();
        db.transaction(function(tx){
            for(i=0;i<=providerRole.length;i++) {
                //var query="select a.providerId,a.server_ProviderID,a.firstname,a.middlename,a.lastname,a.role,a.img,a.gender,a.age,a.mobile,a.email,a.addressType,a.primaryAdd,a.street,a.city,a.state,a.pincode,a.specialization,a.experience,a.status,a.createdAt,a.modifiedAt,b.uniId,b.medical_name,b.delStatus,c.uniId as medID,c.preexistName,c.delStatus as deleteSta from userProviders a left join addMedication b on a.providerId = b.consultantId left join userPreexistCondn c on b.conditionId = c.uniId where role=? and b.delStatus=? and deleteSta=?";

                /* Updated by Anoop */
                //var query="select a.providerId,a.server_ProviderID,a.firstname,a.middlename,a.lastname,a.role,a.img,a.gender,a.age,a.mobile,a.email,a.addressType,a.primaryAdd,a.street,a.city,a.state,a.pincode,a.specialization,a.experience,a.status,a.createdAt,a.modifiedAt,b.uniId,b.medical_name,b.delStatus,c.uniId as medID,c.preexistName,c.delStatus as deleteSta from userProviders a left join addMedication b on a.providerId = b.consultantId and b.delStatus=? and b.server_userID=? left join userPreexistCondn c on b.conditionId = c.uniId and deleteSta=? and c.server_userID=? where a.role=? and a.server_ProviderID=?";

                /* Updated by Anoop for logged in user's provider*/
                var query="select a.providerId,a.server_ProviderID,a.firstname,a.middlename,a.lastname,a.role,a.img,a.gender,a.age,a.mobile,a.email,a.addressType,a.primaryAdd,a.street,a.city,a.state,a.pincode,a.specialization,a.experience,a.status,a.createdAt,a.modifiedAt,b.uniId,b.medical_name,b.delStatus,c.uniId as medID,c.preexistName,c.delStatus as deleteSta from userProviders a left join addMedication b on a.providerId = b.consultantId and b.delStatus=? and b.server_userID=? left join userPreexistCondn c on b.conditionId = c.uniId and deleteSta=? and c.server_userID=? where a.role=? and a.server_ProviderID=?";

                tx.executeSql(query,['active',customService.carouselUserId,'active',customService.carouselUserId,providerRole[i],parseInt(localStorage.getItem('loginUserId'))],function(tx,res){
                    console.log('items => '+JSON.stringify(res.rows));
                    if(res.rows.length>0){
                        for(var i=0;i<res.rows.length;i++){
                            var index = providerArr.findIndex(function(list){
                                return list.uniId == res.rows.item(i).providerId
                            })
                            if(index==-1){
                                providerArr.push({"type":"provider","uniId":res.rows.item(i).providerId,"serverProviderId":res.rows.item(i).server_ProviderID,"firstName":res.rows.item(i).firstname,"middlename":res.rows.item(i).middlename,"lastname":res.rows.item(i).lastname,"role":res.rows.item(i).role,"img":res.rows.item(i).img,"gender":res.rows.item(i).gender,"age":res.rows.item(i).age,"mobile":res.rows.item(i).mobile,"email":res.rows.item(i).email,"addressType":res.rows.item(i).addressType,"primaryAdd":res.rows.item(i).primaryAdd,"street":res.rows.item(i).street,"city":res.rows.item(i).city,"state":res.rows.item(i).state,"pincode":res.rows.item(i).pincode,"specialization":res.rows.item(i).specialization,"experience":res.rows.item(i).experience,"status":res.rows.item(i).status,"createdAt":res.rows.item(i).createdAt,"modifiedAt":res.rows.item(i).modifiedAt});
                                
                                providerArr[providerArr.length-1].medicineArr=[];
                                providerArr[providerArr.length-1].medicineArr.push({medicine:res.rows.item(i).medical_name,medicineId:res.rows.item(i).uniId});
                                providerArr[providerArr.length-1].conditionArr=[];
                                if(res.rows.item(i).medID!=null)
                                    providerArr[providerArr.length-1].conditionArr.push({healthCondnName:res.rows.item(i).preexistName,healthCondnId:res.rows.item(i).medID});
                            }
                            else{
                                providerArr[index].medicineArr.push({medicine:res.rows.item(i).medical_name,medicineId:res.rows.item(i).uniId});
                                if(res.rows.item(i).medID!=null)
                                    providerArr[index].conditionArr.push({healthCondnName:res.rows.item(i).preexistName,healthCondnId:res.rows.item(i).medID})
                            }            
                        }
                    }
                })
            }
        },function(err){
            console.log('providerArr db error: '+JSON.stringify(err));
            deff.reject(err);
        },function(){
            //console.log('providerArr db succ: '+JSON.stringify(providerArr));

            deff.resolve(providerArr);
        })
        return deff.promise;
    }
    
    self.fetchProviderForShare_dr=function(providerRole){
        var providerArr=[];
        var deff=$q.defer();
        db.transaction(function(tx){
            for(i=0;i<=providerRole.length;i++) {
                var query="select a.providerId,a.server_ProviderID,a.firstname,a.middlename,a.lastname,a.role,a.img,a.gender,a.age,a.mobile,a.email,a.addressType,a.primaryAdd,a.street,a.city,a.state,a.pincode,a.specialization,a.experience,a.status,a.createdAt,a.modifiedAt,b.uniId,b.medical_name,b.delStatus,c.uniId as medID,c.preexistName,c.delStatus as deleteSta from userProviders a left join addMedication b on a.providerId = b.consultantId and b.delStatus=? and b.server_userID=? left join userPreexistCondn c on b.conditionId = c.uniId and deleteSta=? and c.server_userID=? where a.role=? and a.server_ProviderID=?";

                tx.executeSql(query,['active',customService.carouselUserId,'active',customService.carouselUserId,providerRole[i],customService.carouselUserId],function(tx,res){
                    console.log('items => '+JSON.stringify(res.rows));
                    if(res.rows.length>0){
                        for(var i=0;i<res.rows.length;i++){
                            var index = providerArr.findIndex(function(list){
                                return list.uniId == res.rows.item(i).providerId
                            })
                            if(index==-1){
                                providerArr.push({"type":"provider","uniId":res.rows.item(i).providerId,"serverProviderId":res.rows.item(i).server_ProviderID,"firstName":res.rows.item(i).firstname,"middlename":res.rows.item(i).middlename,"lastname":res.rows.item(i).lastname,"role":res.rows.item(i).role,"img":res.rows.item(i).img,"gender":res.rows.item(i).gender,"age":res.rows.item(i).age,"mobile":res.rows.item(i).mobile,"email":res.rows.item(i).email,"addressType":res.rows.item(i).addressType,"primaryAdd":res.rows.item(i).primaryAdd,"street":res.rows.item(i).street,"city":res.rows.item(i).city,"state":res.rows.item(i).state,"pincode":res.rows.item(i).pincode,"specialization":res.rows.item(i).specialization,"experience":res.rows.item(i).experience,"status":res.rows.item(i).status,"createdAt":res.rows.item(i).createdAt,"modifiedAt":res.rows.item(i).modifiedAt});
                                
                                providerArr[providerArr.length-1].medicineArr=[];
                                providerArr[providerArr.length-1].medicineArr.push({medicine:res.rows.item(i).medical_name,medicineId:res.rows.item(i).uniId});
                                providerArr[providerArr.length-1].conditionArr=[];
                                if(res.rows.item(i).medID!=null)
                                    providerArr[providerArr.length-1].conditionArr.push({healthCondnName:res.rows.item(i).preexistName,healthCondnId:res.rows.item(i).medID});
                            }
                            else{
                                providerArr[index].medicineArr.push({medicine:res.rows.item(i).medical_name,medicineId:res.rows.item(i).uniId});
                                if(res.rows.item(i).medID!=null)
                                    providerArr[index].conditionArr.push({healthCondnName:res.rows.item(i).preexistName,healthCondnId:res.rows.item(i).medID})
                            }            
                        }
                    }
                })
            }
        },function(err){
            console.log('providerArr db error: '+JSON.stringify(err));
            deff.reject(err);
        },function(){
            //console.log('providerArr db succ: '+JSON.stringify(providerArr));

            deff.resolve(providerArr);
        })
        return deff.promise;
    }

    self.fetchDrAsProviderForPat=function(providerRole){
        var providerArr=[];
        var deff=$q.defer();
        db.transaction(function(tx){
            for(i=0;i<=providerRole.length;i++) {
                var query="select a.providerId,a.server_ProviderID,a.firstname,a.middlename,a.lastname,a.role,a.img,a.gender,a.age,a.mobile,a.email,a.addressType,a.primaryAdd,a.street,a.city,a.state,a.pincode,a.specialization,a.experience,a.status,a.createdAt,a.modifiedAt,b.uniId,b.medical_name,b.delStatus,c.uniId as medID,c.preexistName,c.delStatus as deleteSta from userProviders a left join addMedication b on a.providerId = b.consultantId and b.delStatus=? and b.server_userID=? left join userPreexistCondn c on b.conditionId = c.uniId and deleteSta=? and c.server_userID=? where a.role=? and a.providerId=?";

                tx.executeSql(query,['active',customService.carouselUserId,'active',customService.carouselUserId,providerRole[i],parseInt(localStorage.getItem('proIDForDr'))],function(tx,res){ 
                    console.log('items => '+JSON.stringify(res.rows));
                    if(res.rows.length>0){
                        for(var i=0;i<res.rows.length;i++){
                            var index = providerArr.findIndex(function(list){
                                return list.uniId == res.rows.item(i).providerId
                            })
                            if(index==-1){
                                providerArr.push({"type":"provider","uniId":res.rows.item(i).providerId,"serverProviderId":res.rows.item(i).server_ProviderID,"firstName":res.rows.item(i).firstname,"middlename":res.rows.item(i).middlename,"lastname":res.rows.item(i).lastname,"role":res.rows.item(i).role,"img":res.rows.item(i).img,"gender":res.rows.item(i).gender,"age":res.rows.item(i).age,"mobile":res.rows.item(i).mobile,"email":res.rows.item(i).email,"addressType":res.rows.item(i).addressType,"primaryAdd":res.rows.item(i).primaryAdd,"street":res.rows.item(i).street,"city":res.rows.item(i).city,"state":res.rows.item(i).state,"pincode":res.rows.item(i).pincode,"specialization":res.rows.item(i).specialization,"experience":res.rows.item(i).experience,"status":res.rows.item(i).status,"createdAt":res.rows.item(i).createdAt,"modifiedAt":res.rows.item(i).modifiedAt});
                                
                                providerArr[providerArr.length-1].medicineArr=[];
                                providerArr[providerArr.length-1].medicineArr.push({medicine:res.rows.item(i).medical_name,medicineId:res.rows.item(i).uniId});
                                providerArr[providerArr.length-1].conditionArr=[];
                                if(res.rows.item(i).medID!=null)
                                    providerArr[providerArr.length-1].conditionArr.push({healthCondnName:res.rows.item(i).preexistName,healthCondnId:res.rows.item(i).medID});
                            }
                            else{
                                providerArr[index].medicineArr.push({medicine:res.rows.item(i).medical_name,medicineId:res.rows.item(i).uniId});
                                if(res.rows.item(i).medID!=null)
                                    providerArr[index].conditionArr.push({healthCondnName:res.rows.item(i).preexistName,healthCondnId:res.rows.item(i).medID})
                            }            
                        }
                    }
                })
            }
        },function(err){
            console.log('dr as provider db err');
            deff.reject(err);
        },function(){
            console.log('dr as provider db succ');
            deff.resolve(providerArr);
        })
        return deff.promise;
    }

    self.fetchLowestValueFromTbl = function(tbl,clm){
        var deff=$q.defer();
        var record = [];
        db.transaction(function(tx){
            //var query="SELECT recordDateTime FROM VS_bloodSugar WHERE recordDateTime = (SELECT MIN(recordDateTime) FROM VS_bloodSugar)";
            if(tbl=="VS_eyePower"){
                var query="SELECT "+clm+" as recordDateTime FROM "+tbl+" WHERE server_userID=? AND "+clm+" = (SELECT MIN("+clm+") FROM "+tbl+") LIMIT 1";
            }else{
                var query="SELECT "+clm+" FROM "+tbl+" WHERE server_userID=? AND "+clm+" = (SELECT MIN("+clm+") FROM "+tbl+") LIMIT 1";
            }   
            //var query="SELECT "+clm+" FROM "+tbl+" WHERE "+clm+" = (SELECT MIN("+clm+") FROM "+tbl+") LIMIT 1";        
            tx.executeSql(query,[customService.carouselUserId],function(tx,res){
                if(res.rows.length>0){
                    for(var i=0;i<res.rows.length;i++){
                        record.push(res.rows.item(i));
                    }   
                }
            })
        },function(err){
            console.log('fetch lowest db error');
            deff.reject(err);
        },function(){
            console.log('fetch lowest db succ');
            deff.resolve(record);
        })
        return deff.promise;
    }

    self.selectBetweenData = function(tblName,clmn,fDate,tDate){
        var deff=$q.defer();
        var data = [];
        db.transaction(function(tx){
            var query="select * from "+tblName+" where ("+clmn+" BETWEEN "+fDate+" AND "+tDate+")";
            //console.log("selectWeightData_query->>  "+query);
            tx.executeSql(query,[],function(tx,res){
                //console.log("res.rows==>>  "+JSON.stringify(res.rows));
                if(res.rows.length>0) {
                    for(var i=0;i<res.rows.length;i++){
                        data.push(res.rows.item(i));
                        if(tblName=="VS_bloodPressure")
                            data[data.length-1].type="bp";
                        if(tblName=="VS_bloodSugar")
                            data[data.length-1].type="sugar";
                        if(tblName=="VS_temperature")
                            data[data.length-1].type="temperature";
                        if(tblName=="VS_eyePower")
                            data[data.length-1].type="eyePower";
                        if(tblName=="otherVS")
                            data[data.length-1].type="Symptoms";
                    }
                }
            })
        },function(err){
            console.log('selectBetweenData db error');
            deff.reject(err);
        },function(){
            console.log('selectBetweenData db succ');
            deff.resolve(data);
        })
        return deff.promise;
    }
    
    self.shareData = function(recordArr,objShareArr,userID){
        var deff=$q.defer();
        db.transaction(function(tx){
            for(var i=0;i<recordArr.length;i++){
                tx.executeSql("INSERT INTO shareToUser (server_userID, fromID, toID, name, category, mobile, email, healthCondn, medication, allergy, BP, BS, EP, TEMP, OVS, RSP, Vaccination, Documentation, Emergency, initDate, updtDate, local_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[userID,userID,'',recordArr[i].name,recordArr[i].type,recordArr[i].number,recordArr[i].email,objShareArr.healthCondn,objShareArr.medication,objShareArr.allergy,objShareArr.BP,objShareArr.BS,objShareArr.EP,objShareArr.TEMP,objShareArr.OVS,objShareArr.RSP,objShareArr.Vaccination,objShareArr.Documentation,objShareArr.Emergency,new Date().getTime(),new Date().getTime(),new Date().getTime()],function(txScr,resScr){
                            //succ
                }); 
            }
        },function(err){
            console.log('share data db error');
            deff.reject(err);
        },function(){
            console.log('share data db succ');
            deff.resolve('share success');
        })
        return deff.promise;
    }

    var shortFormatImg=[];
    var count,base64,finalImgArr; 
    var fs;
    self.commonFuncImageStore = function(imageArray){
        var deff=$q.defer();
        count = 0;
        shortFormatImg=[];
        finalImgArr=imageArray;
        console.log('fs => '+customService.fs);
        // console.log('imageArray entry => '+JSON.stringify(imageArray));
        console.log('count => '+count);
        fs=customService.fs;
        if(imageArray.length != 0)
            user.getBase64Image(imageArray[count].img,saveImgToFile); 
        else
           deff.resolve([]); 
        $rootScope.$on('passImgTxt', function (event, args) {
            console.log('message => '+args.message);
            deff.resolve(args.message);
        });
        return deff.promise;
    }
    var saveImgToFile = function(dataUrl){
        base64 = dataUrl;
        //console.log("base64: ",base64);
        var name = new Date().getTime()+".txt"
        shortFormatImg.push({id:finalImgArr[count].id,img:name});
        fs.root.getFile(name, { create: true, exclusive: false }, function(fileEntry){
            fileEntry.createWriter(writerCallback);
        }, 
        function(err){
            console.log("errorwhile getting file: "+JSON.stringify(err));
        });    
    } 
    var writerCallback = function(fileWriter){
        fileWriter.onwriteend = fileWriteCallback;
        fileWriter.onerror = function (e) {
            console.log("Failed file write: " + e.toString());
        };
        fileWriter.write(base64);
    }
    var fileWriteCallback = function(){
        console.log("Successful file write...");
        count++;
        console.log('count => '+count);
        if(count < finalImgArr.length){
          user.getBase64Image(finalImgArr[count].img,saveImgToFile);
        }else{
            var imageNameString=[];
            for(i=0;i<shortFormatImg.length;i++){
                imageNameString.push(shortFormatImg[i]);
                //imageNameString+=","+shortFormatImg[i];
                if(i == shortFormatImg.length-1)
                    $rootScope.$broadcast('passImgTxt', { message: imageNameString });
            }
            console.log('imageNameString => '+imageNameString);
        }
    }
    saveImgToFile = saveImgToFile.bind(this);
    writerCallback = writerCallback.bind(this);
    fileWriteCallback = fileWriteCallback.bind(this);
    var fetchImgArr=[];
    self.commonFuncImageFetch = function(imgageArr,total,currentInd,_deff){
        //console.log('currentInd -> '+currentInd+'\n total -> '+total);
        var deff=_deff?_deff:$q.defer();
        fs=customService.fs;
        if (currentInd < total) {
            fs.root.getFile(imgageArr[currentInd].img, { create: true, exclusive: false }, 
            function (fileEntry) {
                //console.log("file_actual_path: "+fileEntry.toURL());
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function() {
                        //console.log("Successful file read: index => "+currentInd+"\n result => "+this.result);
                        //console.log("Successful file read: index => "+currentInd);
                        fetchImgArr.push({id:imgageArr[currentInd].id,img:this.result});
                        currentInd++;
                        self.commonFuncImageFetch(imgageArr,total,currentInd,deff);
                    };
                    reader.readAsText(file);
                }, function(err){
                    console.log("error while reading file");
                });                     

            },function(err){
            });
        }else{
            //console.log('fetchImgArr => '+JSON.stringify(fetchImgArr));
            deff.resolve(fetchImgArr);
            fetchImgArr=[];
        }
        return deff.promise;
    }


}])

.service('imageConvBase64',function(customService, $q){
    var fetchImgArr=[];
    var fs;
    var self = this;
    self.convertBase64 = function(imgageArr,total,currentInd,_deff){
        //console.log('currentInd -> '+currentInd+'\n total -> '+total);
        var deff=_deff?_deff:$q.defer();
        fs=customService.fs;
        if (currentInd < total) {
            fs.root.getFile(imgageArr[currentInd].img, { create: true, exclusive: false }, 
            function (fileEntry) {
                //console.log("file_actual_path: "+fileEntry.toURL());
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function() {
                        //console.log("Successful file read: index => "+currentInd+"\n result => "+this.result);
                        //console.log("Successful file read: index => "+currentInd);
                        fetchImgArr.push({id:imgageArr[currentInd].id,img:this.result});
                        currentInd++;
                        self.convertBase64(imgageArr,total,currentInd,deff);
                    };
                    reader.readAsText(file);
                }, function(err){
                    console.log("error while reading file");
                });                     

            },function(err){
            });
        }else{
            //console.log('fetchImgArr => '+JSON.stringify(fetchImgArr));
            deff.resolve(fetchImgArr);
            fetchImgArr=[];
        }
        return deff.promise;
    }
})



